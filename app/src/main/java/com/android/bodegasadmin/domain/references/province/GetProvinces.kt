package com.android.bodegasadmin.domain.references.province

import com.android.bodegasadmin.domain.util.Resource

class GetProvinces (private val provinceRepository: ProvinceRepository){
    suspend fun  getProvince(id: String,
                             name: String,
                             department_id: String): Resource<List<Province>> {

        return provinceRepository.getProvinces(id,name,department_id )
    }
}
