package com.android.bodegasadmin.domain.paymentMethod

data class PaymentMethod(
    val paymentMethodId: Int,
    val requestedAmount: Boolean,
    val description: String,
    val customerMessage: String
)