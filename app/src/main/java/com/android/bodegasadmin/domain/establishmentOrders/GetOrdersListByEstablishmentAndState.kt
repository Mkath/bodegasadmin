package com.android.bodegasadmin.domain.establishmentOrders

import com.android.bodegasadmin.domain.establishmentOrders.EstablishmentOrders
import com.android.bodegasadmin.domain.util.Resource

class GetOrdersListByEstablishmentAndState(private val establishmentOrderRepository: EstablishmentOrderRepository) {
    suspend fun getOrdersListByEstablishmentAndState(establishmentId: Int,
                                              orderState: String) : Resource<List<EstablishmentOrders>> {
        return establishmentOrderRepository.getOrdersListByEstablishmentAndState(establishmentId, orderState)
    }
}