package com.android.bodegasadmin.domain.products_template

data class Products(
    val productTemplateId: Int,
    val code: String,
    val name: String,
    val description: String,
    val unitMeasure: String,
    val imageProduct: String?="",
    val tag: String
)