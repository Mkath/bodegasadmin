package com.android.bodegasadmin.domain.products_template

data class ProductsBody(
    val productTemplateId: Int,
    val price: String
)