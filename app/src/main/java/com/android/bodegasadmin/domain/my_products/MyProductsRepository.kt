package com.android.bodegasadmin.domain.my_products

import com.android.bodegasadmin.domain.util.Resource


interface MyProductsRepository {

    suspend fun getMyProducts(
        searchText: String
    ): Resource<List<MyProducts>>

    suspend fun updateMyProduct(
        storeProductId:Int,
        price:Double,
        stock:String
    ): Resource<Boolean>
}
