package com.android.bodegasadmin.domain.references.district

import com.android.bodegasadmin.domain.util.Resource


class GetDistricts (private val districtRepository: DistrictRepository){
    suspend fun getDistrict(id: String,
                            name: String,
                            province_id: String,
                            department_id: String): Resource<List<District>> {
        return districtRepository.getDistricts(id,name, province_id,department_id )
    }
}

