package com.android.bodegasadmin.domain.distance

import com.android.bodegasadmin.domain.util.Resource

class GetDistance constructor(private val distanceRepository: DistanceRepository) {

    suspend fun getDistance(eLat: Double, eLong: Double, cLat :Double,  cLong:Double): Resource<Distance> {
        return distanceRepository.getDistance(eLat, eLong, cLat, cLong)
    }
}