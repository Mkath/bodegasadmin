package com.android.bodegasadmin.domain.my_products

import com.android.bodegasadmin.domain.util.Resource
import java.util.*


class GetMyProducts(private val productsRepository: MyProductsRepository) {

    suspend fun getMyProducts(
        searchText: String
    ): Resource<List<MyProducts>> {
        return productsRepository.getMyProducts(searchText)
    }
}
