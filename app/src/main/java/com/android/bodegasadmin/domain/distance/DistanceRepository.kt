package com.android.bodegasadmin.domain.distance

import com.android.bodegasadmin.domain.util.Resource

interface DistanceRepository {

    suspend fun getDistance(eLat: Double, eLong: Double, cLat :Double,  cLong:Double): Resource<Distance>
}