package com.android.bodegasadmin.domain.distance

data class Distance(
    val distanceInKms: String,
    val numberOfBlocks: Int,
    val travelTimeByCar: String,
    val travelTimeByBike: String,
    val travelTimeByWalking: String
)