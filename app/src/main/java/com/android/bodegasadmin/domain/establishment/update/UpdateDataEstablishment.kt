package com.android.bodegasadmin.domain.establishment.update

import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody

class UpdateDataEstablishment constructor(private val userRepository: EstablishmentRepository) {

    suspend fun updateEstablishment(customerId: Int, body: UpdateEstablishmentBody): Resource<Establishment> {
        return userRepository.updateEstablishment(customerId, body)
    }
}