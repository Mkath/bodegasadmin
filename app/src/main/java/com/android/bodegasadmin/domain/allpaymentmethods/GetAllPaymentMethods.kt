package com.android.bodegasadmin.domain.allpaymentmethods

import com.android.bodegasadmin.domain.util.Resource

class GetAllPaymentMethods constructor(private val allPaymentMethodsRepository: AllPaymentMethodRepository) {

    suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethod>> {
        return allPaymentMethodsRepository.getAllPaymentMethods()
    }
}