package com.android.bodegasadmin.domain.establishment

class EstablishmentLoginToken (
    val idEntity: String,
    val token: String
)