package com.android.bodegasadmin.domain.allschedule

import com.android.bodegasadmin.domain.establishment.register.RegisterEstablishmentBody
import com.android.bodegasadmin.domain.util.Resource


interface AllScheduleRepository {

    suspend fun getAllSchedule(type:String): Resource<List<AllSchedule>>

}
