package com.android.bodegasadmin.domain.products_template

import com.android.bodegasadmin.domain.util.Resource


interface ProductsRepository {
    suspend fun getProducts(text:String): Resource<List<Products>>

    suspend fun addProducts(productId:Int, productPrice:String) : Resource<Boolean>

}
