package com.android.bodegasadmin.domain.establishment.login

import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource


class LoginEstablishment constructor(private val establishmentRepository: EstablishmentRepository) {

    suspend fun getDataEstablishment(establishmentId: Int): Resource<Establishment> {
        return establishmentRepository.loginEstablishment(establishmentId)
    }
}