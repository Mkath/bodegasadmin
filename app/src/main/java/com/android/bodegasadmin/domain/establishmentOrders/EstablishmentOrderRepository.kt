package com.android.bodegasadmin.domain.establishmentOrders

import com.android.bodegasadmin.domain.establishmentOrders.EstablishmentOrders
import com.android.bodegasadmin.domain.util.Resource

interface EstablishmentOrderRepository {
    suspend fun getOrdersListByEstablishmentAndState(
        establishmentId: Int,
        orderState: String
    ): Resource<List<EstablishmentOrders>>

    suspend fun updateStateOrder(
        deliveryCharge: Double,
        orderId:Int,
        state:String,
        list: List<OrderDetailBody>
    ): Resource<Boolean>
}