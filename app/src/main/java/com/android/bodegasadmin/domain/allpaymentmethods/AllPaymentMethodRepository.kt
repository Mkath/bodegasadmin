package com.android.bodegasadmin.domain.allpaymentmethods

import com.android.bodegasadmin.domain.allschedule.AllSchedule
import com.android.bodegasadmin.domain.util.Resource

interface AllPaymentMethodRepository {

    suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethod>>
}