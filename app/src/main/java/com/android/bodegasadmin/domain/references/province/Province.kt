package com.android.bodegasadmin.domain.references.province

data class Province (
    val id: String,
    val name: String,
    val department_id: String
)