package com.android.bodegasadmin.domain.allschedule

import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.util.Resource


class GetAllScheduleByType constructor(private val allScheduleRepository: AllScheduleRepository) {

    suspend fun getAllScheduleByType(type: String): Resource<List<AllSchedule>> {
        return allScheduleRepository.getAllSchedule(type)
    }
}