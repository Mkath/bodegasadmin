package com.android.bodegasadmin.domain.references.department

import com.android.bodegasadmin.domain.util.Resource

class GetDepartments (private val departmentRepository: DepartmentRepository){
    suspend fun getDepartments(id:String, name: String) : Resource<List<Department>> {
        return departmentRepository.getDepartments(id, name)
    }
}