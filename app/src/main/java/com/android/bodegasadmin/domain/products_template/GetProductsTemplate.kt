package com.android.bodegasadmin.domain.products_template

import com.android.bodegasadmin.domain.util.Resource


class GetProductsTemplate(private val productsRepository: ProductsRepository) {

    suspend fun getProductsTemplate(text:String): Resource<List<Products>> {
        return productsRepository.getProducts(text)
    }

}
