package com.android.bodegasadmin.domain.allschedule

data class AllSchedule(
    val scheduleId: Int,
    val range: String,
    val type: String,
    val startTime: String ? ="",
    val endTime: String ? = ""
)