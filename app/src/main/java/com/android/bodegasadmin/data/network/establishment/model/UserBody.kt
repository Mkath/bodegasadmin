package com.android.bodegasadmin.data.network.establishment.model

import com.google.gson.annotations.SerializedName

class UserBody(
    val email: String,
    val password: String,
    @SerializedName("userType")
    val userType: String = "E")
