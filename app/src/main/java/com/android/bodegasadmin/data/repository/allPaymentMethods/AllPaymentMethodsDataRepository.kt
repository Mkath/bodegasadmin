package com.android.bodegasadmin.data.repository.allPaymentMethods

import com.android.bodegasadmin.data.repository.allPaymentMethods.mapper.AllPaymentMethodsMapper
import com.android.bodegasadmin.data.repository.allPaymentMethods.store.remote.AllPaymentMethodsRemoteDataStore
import com.android.bodegasadmin.domain.allpaymentmethods.AllPaymentMethod
import com.android.bodegasadmin.domain.allpaymentmethods.AllPaymentMethodRepository
import com.android.bodegasadmin.domain.util.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class AllPaymentMethodsDataRepository constructor(
    private val allPaymentMethodsRemoteDataStore: AllPaymentMethodsRemoteDataStore,
    private val allPaymentMethodsMapper: AllPaymentMethodsMapper
) : AllPaymentMethodRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethod>> {
        return coroutineScope {
            val allScheduleEntity = allPaymentMethodsRemoteDataStore.getAllPaymentMethods()
            val allScheduleList = mutableListOf<AllPaymentMethod>()
            allScheduleEntity.data.forEach {
                allScheduleList.add(allPaymentMethodsMapper.mapFromEntity(it))
            }
            return@coroutineScope Resource(
                allScheduleEntity.status,
                allScheduleList,
                allScheduleEntity.message
            )
        }
    }

}