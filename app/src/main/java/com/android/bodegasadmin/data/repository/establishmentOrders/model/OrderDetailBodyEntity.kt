package com.android.bodegasadmin.data.repository.establishmentOrders.model

class OrderDetailBodyEntity (
    val orderDetailId: Int,
    val state: String
)