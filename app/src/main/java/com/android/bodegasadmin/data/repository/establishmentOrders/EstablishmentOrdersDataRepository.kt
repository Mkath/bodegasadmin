package com.android.bodegasadmin.data.repository.establishmentOrders

import android.util.Log
import com.android.bodegasadmin.data.repository.establishmentOrders.mapper.EstablishmentOrdersMapper
import com.android.bodegasadmin.data.repository.establishmentOrders.mapper.OrderDetailBodyMapper
import com.android.bodegasadmin.data.repository.establishmentOrders.model.EstablishmentOrdersEntity
import com.android.bodegasadmin.domain.establishmentOrders.EstablishmentOrders
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailBodyEntity
import com.android.bodegasadmin.data.repository.establishmentOrders.store.remote.EstablishmentOrdersRemoteDataStorage
import com.android.bodegasadmin.domain.establishmentOrders.EstablishmentOrderRepository
import com.android.bodegasadmin.domain.establishmentOrders.OrderDetailBody
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class EstablishmentOrdersDataRepository (
    private val establishmentOrdersRemoteDataStore: EstablishmentOrdersRemoteDataStorage,
    private val establishmentOrdersMapper: EstablishmentOrdersMapper,
    private val orderDetailBodyMapper: OrderDetailBodyMapper
) : EstablishmentOrderRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getOrdersListByEstablishmentAndState(establishmentId: Int,
                                                       orderState: String): Resource<List<EstablishmentOrders>> {
        return coroutineScope {
            val customerOrdersList = establishmentOrdersRemoteDataStore.getOrdersListByEstablishmentAndState(establishmentId, orderState)
            var resource : Resource<List<EstablishmentOrders>> = Resource(customerOrdersList.status, mutableListOf(), customerOrdersList.message)
            if(customerOrdersList.status == Status.SUCCESS)  {
                val mutableListcustomerOrders = mapClassList(customerOrdersList.data)
                Log.e("Debug", "-----------------------------------------------------------")
                Log.e("Debug", " en capa repository: "+ mutableListcustomerOrders.toString())
                Log.e("Debug", "-----------------------------------------------------------")
                resource = Resource(Status.SUCCESS, mutableListcustomerOrders, customerOrdersList.message)
                //Log.e("Debug", "-----------------------------------------------------------")
                //Log.e("Debug", " en capa repository resource: "+ resource.toString())
               // Log.e("Debug", "-----------------------------------------------------------")
            }
            else {
                if(customerOrdersList.status == Status.ERROR) {
                    resource = Resource(customerOrdersList.status, mutableListOf(), customerOrdersList.message)
                }
            }
            return@coroutineScope resource
        }
    }

    private fun mapClassList(customerOrdersEntityList: List<EstablishmentOrdersEntity>) : List<EstablishmentOrders> {
        val customerOrdersList = mutableListOf<EstablishmentOrders>()
        customerOrdersEntityList.forEach{
            customerOrdersList.add(establishmentOrdersMapper.mapFromEntity(it))
        }
        return customerOrdersList
    }

    override suspend fun updateStateOrder(
        deliveryCharge: Double,
        orderId: Int,
        state: String,
        list: List<OrderDetailBody>
    ): Resource<Boolean> {
        return coroutineScope {
            val sendList = mutableListOf<OrderDetailBodyEntity>()
            list.forEach {
                sendList.add(orderDetailBodyMapper.mapToEntity(it))
            }
            val result = establishmentOrdersRemoteDataStore.updateStateOrder(deliveryCharge,orderId, state, sendList)
            return@coroutineScope Resource(result.status, result.data, result.message)
        }    }
}