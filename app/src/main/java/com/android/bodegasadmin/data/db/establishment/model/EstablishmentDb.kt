package com.android.bodegasadmin.data.db.establishment.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.bodegasadmin.data.db.establishment.dao.EstablishmentConstants

@Entity(tableName = EstablishmentConstants.TABLE_NAME)
data class EstablishmentDb(
    val establishmentId: Int,
    val address: String,
    val businessName: String,
    val cardNumber: String,
    val cardOperator: String,
    val departament: String,
    val district: String,
    val dni: String,
    val email: String,
    val lastNameMaternal: String,
    val lastNamePaternal: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val province: String,
    val ruc: String,
    val status: String,
    val urbanization: String,
    val delivery: String,
    val pathImage: String? = "",
    val establishmentType: String,
    val deliveryCharge: Boolean
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}
