package com.android.bodegasadmin.data.network.products_template.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProductsBodySend(
    @SerializedName("products")
    @Expose
    val products: List<ProductBodyItem>
)

class ProductBodyItem(
    @SerializedName("price")
    @Expose
    val price: String,
    @SerializedName("productTemplateId")
    @Expose
    val productTemplateId: Int
)