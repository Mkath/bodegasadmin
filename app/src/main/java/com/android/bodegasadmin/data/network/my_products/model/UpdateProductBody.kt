package com.android.bodegasadmin.data.network.my_products.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UpdateProductBody(
    @SerializedName("price")
    @Expose
    val price: Double,
    @SerializedName("stock")
    @Expose
    val stock: String
)