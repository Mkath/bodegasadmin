package com.android.bodegasadmin.data.repository.establishment.store.db

import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.domain.util.Resource

interface EstablishmentDb {

    suspend fun getEstablishment(): Resource<EstablishmentEntity>

    suspend fun saveEstablishment(establishmentEntity: EstablishmentEntity)

    suspend fun clearAllTables()
}