package com.android.bodegasadmin.data.network.my_products.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MyProductsResponse(
    @SerializedName("price")
    @Expose
    val price: Double,
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("storeProductId")
    @Expose
    val storeProductId: Int,
    @SerializedName("stock")
    @Expose
    val stock: String,
    @SerializedName("productTemplate")
    @Expose
    val productDetails: MyProductsDetailsResponse
)