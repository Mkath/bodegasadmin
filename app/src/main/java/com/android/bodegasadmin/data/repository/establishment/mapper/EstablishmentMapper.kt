package com.android.bodegasadmin.data.repository.establishment.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.PaymentMethodEntity
import com.android.bodegasadmin.data.repository.establishment.model.ScheduleEntity
import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.paymentMethod.PaymentMethod
import com.android.bodegasadmin.domain.schedule.Schedule


class EstablishmentMapper(
    private val scheduleMapper: ScheduleMapper,
    private val paymentMethodMapper: PaymentMethodMapper
) : Mapper<EstablishmentEntity, Establishment> {

    override fun mapFromEntity(type: EstablishmentEntity): Establishment {
        return Establishment(
            type.establishmentId,
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.delivery,
            type.pathImage,
            getListSchedules(type.shippingSchedule!!),
            getListSchedules(type.operationSchedule),
            type.establishmentType,
            type.deliveryCharge,
            getListPaymentMethods(type.paymentMethod)
        )
    }

    override fun mapToEntity(type: Establishment): EstablishmentEntity {
        return EstablishmentEntity(
            type.establishmentId,
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.delivery,
            type.pathImage,
            getListSchedulesFromUsesCase(type.shippingSchedule),
            getListSchedulesFromUsesCase(type.operationSchedule),
            type.establishmentType,
            type.deliveryCharge,
            getListPaymentMethodsFromUsesCase(type.paymentMethod)
        )
    }


    private fun getListSchedules(list: List<ScheduleEntity>): List<Schedule>{
        val listSchedule = mutableListOf<Schedule>()
        list.forEach {
            listSchedule.add(scheduleMapper.mapFromEntity(it))
        }
        return listSchedule
    }

    private fun getListSchedulesFromUsesCase(list: List<Schedule>): List<ScheduleEntity>{
        val listSchedule = mutableListOf<ScheduleEntity>()
        list.forEach {
            listSchedule.add(scheduleMapper.mapToEntity(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethods(list: List<PaymentMethodEntity>): List<PaymentMethod>{
        val listPaymentMethods = mutableListOf<PaymentMethod>()
        list.forEach {
            listPaymentMethods.add(paymentMethodMapper.mapFromEntity(it))
        }
        return listPaymentMethods
    }

    private fun getListPaymentMethodsFromUsesCase(list: List<PaymentMethod>): List<PaymentMethodEntity>{
        val listPaymentMethods = mutableListOf<PaymentMethodEntity>()
        list.forEach {
            listPaymentMethods.add(paymentMethodMapper.mapToEntity(it))
        }
        return listPaymentMethods
    }
}