package com.android.bodegasadmin.data.repository.allschedules

import com.android.bodegasadmin.data.repository.allschedules.mapper.AllScheduleMapper
import com.android.bodegasadmin.data.repository.allschedules.store.remote.AllSchedulesRemoteDataStore
import com.android.bodegasadmin.data.repository.establishment.mapper.EstablishmentMapper
import com.android.bodegasadmin.data.repository.establishment.mapper.RegisterEstablishmentMapper
import com.android.bodegasadmin.data.repository.establishment.mapper.ScheduleMapper
import com.android.bodegasadmin.data.repository.establishment.store.db.EstablishmentDbDataStore
import com.android.bodegasadmin.data.repository.establishment.store.remote.EstablishmentRemoteDataStore
import com.android.bodegasadmin.domain.allschedule.AllSchedule
import com.android.bodegasadmin.domain.allschedule.AllScheduleRepository
import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.establishment.EstablishmentRepository
import com.android.bodegasadmin.domain.establishment.register.RegisterEstablishmentBody
import com.android.bodegasadmin.domain.util.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class AllSchedulesDataRepository constructor(
    private val allSchedulesRemoteDataStore: AllSchedulesRemoteDataStore,
    private val allScheduleMapper: AllScheduleMapper
) : AllScheduleRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getAllSchedule(type: String): Resource<List<AllSchedule>> {
        return coroutineScope {
            val allScheduleEntity = allSchedulesRemoteDataStore.getAllSchedules(type)
            val allScheduleList = mutableListOf<AllSchedule>()
            allScheduleEntity.data.forEach {
                allScheduleList.add(allScheduleMapper.mapFromEntity(it))
            }
            return@coroutineScope Resource(
                allScheduleEntity.status,
                allScheduleList,
                allScheduleEntity.message
            )
        }
    }

}