package com.android.bodegasadmin.data.network.searchOrders.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.establishmentOrders.mapper.EstablishmentDetailItemEntityMapper
import com.android.bodegasadmin.data.network.establishmentOrders.model.OrderDetailItem
import com.android.bodegasadmin.data.network.searchOrders.model.SearchListOrdersBodyResponse
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailItemEntity
import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersEntity

class SearchOrdersEntityMapper(
    private val searchOrdersDetailItemsEntityMapper: EstablishmentDetailItemEntityMapper
) :
    Mapper<SearchListOrdersBodyResponse, SearchOrdersEntity> {

    override fun mapFromRemote(type: SearchListOrdersBodyResponse): SearchOrdersEntity {
        val ammount = if (type.customerAmount.isNullOrEmpty()){
            "0.00"
        }else{
            type.customerAmount
        }
        val delivery = if (type.deliveryCharge.isNullOrEmpty()){
            "0.00"
        }else{
            type.deliveryCharge
        }

        val message = if (type.paymentMethod.establishmentMessage.isNullOrEmpty()){
            ""
        }else{
            type.paymentMethod.establishmentMessage
        }
        return SearchOrdersEntity(
            type.creationDate.orEmpty(),
            type.orderId.toString(),
            type.deliveryType,
            type.shippingDateFrom.orEmpty(),
            type.shippingDateUntil.orEmpty(),
            type.status,
            type.establishment.establishmentId.toString(),
            type.establishment.email,
            type.establishment.name,
            type.establishment.phoneNumber,
            type.total,
            type.customer.phoneNumber,
            type.customer.name,
            type.customer.lastNamePaternal,
            type.customer.lastNameMaternal,
            type.customer.address,
            type.customer.latitude,
            type.customer.longitude,
            getOrdersDetail(type.orderDetail),
            type.updateDate,
            type.paymentMethod.customerMessage,
            type.paymentMethod.description,
            message,
            type.paymentMethod.paymentMethodId,
            type.paymentMethod.requestedAmount,
            ammount,
            delivery,
            type.customer.urbanization
        )
    }

    private fun getOrdersDetail(list: List<OrderDetailItem>): List<OrderDetailItemEntity> {
        val listOrdersDetail = mutableListOf<OrderDetailItemEntity>()
        list.forEach {
            listOrdersDetail.add(searchOrdersDetailItemsEntityMapper.mapFromRemote(it))
        }
        return listOrdersDetail
    }
}