package com.android.bodegasadmin.data.repository.establishment.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentLoginEntity
import com.android.bodegasadmin.domain.establishment.EstablishmentLoginToken

class EstablishmentLoginMapper: Mapper<EstablishmentLoginEntity, EstablishmentLoginToken> {

    override fun mapFromEntity(type: EstablishmentLoginEntity): EstablishmentLoginToken {
        return EstablishmentLoginToken(
            type.id_entity,
            type.token
        )
    }

    override fun mapToEntity(type: EstablishmentLoginToken): EstablishmentLoginEntity {
        return EstablishmentLoginEntity(
            type.idEntity,
            type.token
        )
    }
}