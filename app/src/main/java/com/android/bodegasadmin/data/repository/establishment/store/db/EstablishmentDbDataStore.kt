package com.android.bodegasadmin.data.repository.establishment.store.db

import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentLoginEntity
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity
import com.android.bodegasadmin.data.repository.establishment.store.EstablishmentDataStore
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.login.ForgetPassDTO
import com.android.bodegasadmin.presentation.profile.ChangePasswordDtoBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentScheduledDTO

class EstablishmentDbDataStore constructor(
    private val establishmentDb: EstablishmentDb
) : EstablishmentDataStore {

    override suspend fun loginEstablishmentToken(
        email: String,
        password: String
    ): Resource<EstablishmentLoginEntity> {
        throw UnsupportedOperationException()

    }

    override suspend fun loginEstablishment(
        establishmentId: Int
    ): Resource<EstablishmentEntity> {
        throw UnsupportedOperationException()

    }

    override suspend fun saveEstablishment(establishmentEntity: EstablishmentEntity) {
        establishmentDb.saveEstablishment(establishmentEntity)
    }

    override suspend fun clearEstablishment() {
        establishmentDb.clearAllTables()
    }

    override suspend fun getEstablishment(): Resource<EstablishmentEntity> {
        return establishmentDb.getEstablishment()
    }

    override suspend fun registerEstablishment(registerEstablishmentBodyEntity: RegisterEstablishmentBodyEntity): Resource<EstablishmentLoginEntity> {
        throw UnsupportedOperationException()
    }

    override suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean> {
        throw UnsupportedOperationException()
    }

    override suspend fun updateEstablishment(
        customerId: Int,
        body: UpdateEstablishmentBody
    ): Resource<EstablishmentEntity> {
        throw UnsupportedOperationException()
    }

    override suspend fun updatePassword(
        body: ChangePasswordDtoBody
    ): Resource<Boolean> {
        throw UnsupportedOperationException()
    }

    override suspend fun recoveryPassword( body: ForgetPassDTO): Resource<Boolean> {
        throw UnsupportedOperationException()
    }

    override suspend fun updateSchedule(establishmentId:Int,  body: UpdateEstablishmentScheduledDTO): Resource<Boolean> {
        throw UnsupportedOperationException()
    }
}