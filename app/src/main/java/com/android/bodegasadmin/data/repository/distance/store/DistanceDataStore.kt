package com.android.bodegasadmin.data.repository.distance.store

import com.android.bodegasadmin.data.repository.distance.model.DistanceEntity
import com.android.bodegasadmin.domain.util.Resource

interface DistanceDataStore {

    suspend fun getDistance(eLat: Double, eLong: Double, cLat :Double, cLong: Double): Resource<DistanceEntity>
}