package com.android.bodegasadmin.data.repository.searchOrders.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishmentOrders.mapper.EstablishmentDetailItemMapper
import com.android.bodegasadmin.data.repository.establishmentOrders.mapper.EstablishmentOrdersMapper
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailItemEntity
import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersDetailItemEntity
import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersEntity
import com.android.bodegasadmin.domain.establishmentOrders.OrderDetails
import com.android.bodegasadmin.domain.searchOrders.SearchOrderDetails
import com.android.bodegasadmin.domain.searchOrders.SearchEstablishmentOrders

class SearchOrdersMapper (
    private val establishmentItemMapper: EstablishmentDetailItemMapper
): Mapper<SearchOrdersEntity, SearchEstablishmentOrders> {
    override fun mapFromEntity(type: SearchOrdersEntity): SearchEstablishmentOrders {
        return SearchEstablishmentOrders(
            type.creationDate,
            type.orderId,
            type.deliveryType,
            type.shippingDateFrom,
            type.shippingDateUntil,
            type.status,
            type.establishmentId,
            type.establishmentEmail,
            type.establishmentName,
            type.establishmentPhoneNumber,
            type.total,
            type.customerPhoneNumber,
            type.customerName,
            type.customerLastNamePaternal,
            type.customerLastNameMaternal,
            type.customerAddress,
            type.customerLatitude,
            type.customerLongitude,
            getOrdersDetail(type.orderDetails),
            type.updateUser,
            type.paymentMethodCustomerMessage,
            type.paymentMethodDescription,
            type.paymentMethodEstablishmentMessage,
            type.paymentMethodPaymentMethodId,
            type.paymentMethodRequestAmount,
            type.customerAmount,
            type.deliveryCharge,
            type.customerUrbanization
        )
    }

    override fun mapToEntity(type: SearchEstablishmentOrders): SearchOrdersEntity {
        return SearchOrdersEntity(
            type.creationDate,
            type.orderId,
            type.deliveryType,
            type.shippingDateFrom,
            type.shippingDateUntil,
            type.status,
            type.establishmentId,
            type.establishmentEmail,
            type.establishmentName,
            type.establishmentPhoneNumber,
            type.total,
            type.customerPhoneNumber,
            type.customerName,
            type.customerLastNamePaternal,
            type.customerLastNameMaternal,
            type.customerAddress,
            type.customerLatitude,
            type.customerLongitude,
            getOrdersDetailFromUsesCase(type.orderDetails),
            type.updateUser,
            type.paymentMethodCustomerMessage,
            type.paymentMethodDescription,
            type.paymentMethodEstablishmentMessage,
            type.paymentMethodPaymentMethodId,
            type.paymentMethodRequestAmount,
            type.customerAmount,
            type.deliveryCharge,
            type.customerUrbanization
        )
    }

    private fun getOrdersDetail(list: List<OrderDetailItemEntity>): List<OrderDetails>{
        val listSchedule = mutableListOf<OrderDetails>()
        list.forEach {
            listSchedule.add(establishmentItemMapper.mapFromEntity(it))
        }
        return listSchedule
    }

    private fun getOrdersDetailFromUsesCase(list: List<OrderDetails>): List<OrderDetailItemEntity>{
        val listSchedule = mutableListOf<OrderDetailItemEntity>()
        list.forEach {
            listSchedule.add(establishmentItemMapper.mapToEntity(it))
        }
        return listSchedule
    }
}