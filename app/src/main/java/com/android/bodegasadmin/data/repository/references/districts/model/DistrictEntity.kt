package com.android.bodegasadmin.data.repository.references.districts.model

class DistrictEntity (
    val id: String,
    val name: String,
    val province_id: String,
    val department_id: String
)