package com.android.bodegasadmin.data.repository.references.districts.store.remote

import com.android.bodegasadmin.data.repository.references.districts.model.DistrictEntity
import com.android.bodegasadmin.domain.util.Resource

interface DistrictRemote {
    suspend fun getDistricts (
        id: String,
        name: String,
        province_id: String,
        department_id: String
    ): Resource<List<DistrictEntity>>
}
