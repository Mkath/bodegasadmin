package com.android.bodegasadmin.data.repository.distance.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.distance.model.DistanceEntity
import com.android.bodegasadmin.domain.distance.Distance

class DistanceMapper : Mapper<DistanceEntity, Distance> {

    override fun mapFromEntity(type: DistanceEntity): Distance {
        return Distance(
            type.distanceInKms,
            type.numberOfBlocks,
            type.travelTimeByCar,
            type.travelTimeByBike,
            type.travelTimeByWalking
        )
    }

    override fun mapToEntity(type: Distance): DistanceEntity {
        return DistanceEntity(
            type.distanceInKms,
            type.numberOfBlocks,
            type.travelTimeByCar,
            type.travelTimeByBike,
            type.travelTimeByWalking
        )
    }

}