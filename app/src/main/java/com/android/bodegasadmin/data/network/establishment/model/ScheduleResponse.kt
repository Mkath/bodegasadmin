package com.android.bodegasadmin.data.network.establishment.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ScheduleResponse(
    @SerializedName("range")
    @Expose
    val range: String,
    @SerializedName("startTime")
    @Expose
    val startTime: String ? = "",
    @SerializedName("endTime")
    @Expose
    val endTime: String ? = "",
    @SerializedName("scheduleId")
    @Expose
    val shippingScheduleId: Int


)