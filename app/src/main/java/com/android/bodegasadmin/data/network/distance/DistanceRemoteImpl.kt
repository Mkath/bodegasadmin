package com.android.bodegasadmin.data.network.distance

import android.content.Context
import com.android.bodegasadmin.data.network.distance.mapper.DistanceEntityMapper
import com.android.bodegasadmin.data.repository.distance.model.DistanceEntity
import com.android.bodegasadmin.data.repository.distance.store.remote.DistanceRemote
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext

class DistanceRemoteImpl constructor(
    private val distanceServices: DistanceServices,
    private val distanceEntityMapper: DistanceEntityMapper
) : DistanceRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDistance(eLat: Double, eLong: Double, cLat :Double,  cLong:Double): Resource<DistanceEntity>
    {
        return coroutineScope {
            try {
                val response = distanceServices.getDistance(eLat, eLong, cLat, cLong)
                if (response.isSuccessful) {
                    if(response.body()!!.success){
                        val allPaymentMethodsResponse = response.body()!!.data!!.distance
                        val allPaymentMethodsEntity = distanceEntityMapper.mapFromRemote(allPaymentMethodsResponse)

                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            allPaymentMethodsEntity,
                            response.message()
                        )
                    }
                    else {
                        val distanceEntity = DistanceEntity("", 0, "", "", "")
                        val message = response.body()!!.message
                        return@coroutineScope Resource(
                            Status.ERROR,
                            distanceEntity,
                            message
                        )
                    }
                } else {
                    val distanceEntity = DistanceEntity("", 0, "", "", "")
                    return@coroutineScope Resource(
                        Status.ERROR,
                        distanceEntity,
                        response.message()
                    )
                }
            } catch (e: UnknownHostException) {
                val distanceEntity = DistanceEntity("", 0, "", "", "")
                return@coroutineScope Resource(
                    Status.ERROR,
                    distanceEntity,
                    "500"
                )
            } catch (e: Throwable) {
                val distanceEntity = DistanceEntity("", 0, "", "", "")
                return@coroutineScope Resource(
                    Status.ERROR,
                    distanceEntity,
                    e.message
                )
            }
        }
    }
}