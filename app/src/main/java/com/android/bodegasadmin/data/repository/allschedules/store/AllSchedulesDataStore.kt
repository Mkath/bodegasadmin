package com.android.bodegasadmin.data.repository.allschedules.store

import com.android.bodegasadmin.data.repository.allschedules.model.AllScheduleEntity
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity
import com.android.bodegasadmin.domain.util.Resource

interface AllSchedulesDataStore {

    suspend fun getAllSchedules(type:String): Resource<List<AllScheduleEntity>>

}