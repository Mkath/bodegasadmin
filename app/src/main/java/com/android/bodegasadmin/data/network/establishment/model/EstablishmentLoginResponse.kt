package com.android.bodegasadmin.data.network.establishment.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EstablishmentLoginResponse(
    @SerializedName("idEntity")
    @Expose
    val idEntity: String,
    @SerializedName("token")
    @Expose
    val token: String
)