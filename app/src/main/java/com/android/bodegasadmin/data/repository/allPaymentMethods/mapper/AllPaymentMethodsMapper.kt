package com.android.bodegasadmin.data.repository.allPaymentMethods.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity
import com.android.bodegasadmin.domain.allpaymentmethods.AllPaymentMethod

class AllPaymentMethodsMapper : Mapper<AllPaymentMethodsEntity, AllPaymentMethod> {

    override fun mapFromEntity(type: AllPaymentMethodsEntity): AllPaymentMethod {
        return AllPaymentMethod(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

    override fun mapToEntity(type: AllPaymentMethod): AllPaymentMethodsEntity {
        return AllPaymentMethodsEntity(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}