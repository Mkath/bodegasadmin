package com.android.bodegasadmin.data.repository.distance

import com.android.bodegasadmin.data.repository.distance.mapper.DistanceMapper
import com.android.bodegasadmin.data.repository.distance.store.remote.DistanceRemoteDataStore
import com.android.bodegasadmin.domain.distance.Distance
import com.android.bodegasadmin.domain.distance.DistanceRepository
import com.android.bodegasadmin.domain.util.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class DistanceDataRepository constructor(
    private val distanceRemoteDataStore: DistanceRemoteDataStore,
    private val allScheduleMapper: DistanceMapper
) : DistanceRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDistance(eLat: Double, eLong: Double, cLat :Double,  cLong:Double): Resource<Distance> {
        return coroutineScope {
            val distanceEntity = distanceRemoteDataStore.getDistance(eLat, eLong, cLat, cLong)
            val distance = allScheduleMapper.mapFromEntity(distanceEntity.data)
            return@coroutineScope Resource(
                distanceEntity.status,
                distance,
                distanceEntity.message
            )
        }
    }

}