package com.android.bodegasadmin.data.network.establishmentOrders.mapper

import com.android.bodegasadmin.data.network.establishmentOrders.model.EstablishmentListOrdersBodyResponse
import com.android.bodegasadmin.data.network.establishmentOrders.model.OrderDetailItem
import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.repository.establishmentOrders.model.EstablishmentOrdersEntity
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailItemEntity

class EstablishmentOrdersEntityMapper(
    private val establishmentDetailItemsEntityMapper: EstablishmentDetailItemEntityMapper
) :
    Mapper<EstablishmentListOrdersBodyResponse, EstablishmentOrdersEntity> {

    override fun mapFromRemote(type: EstablishmentListOrdersBodyResponse): EstablishmentOrdersEntity {
        val ammount = if (type.customerAmount.isNullOrEmpty()){
            "0.00"
        }else{
            type.customerAmount
        }
        val delivery = if (type.deliveryCharge.isNullOrEmpty()){
            "0.00"
        }else{
            type.deliveryCharge
        }
        return EstablishmentOrdersEntity(
            type.creationDate,
            type.orderId.toString(),
            type.deliveryType,
            type.shippingDateFrom.orEmpty(),
            type.shippingDateUntil.orEmpty(),
            type.status,
            type.establishment.establishmentId.toString(),
            type.establishment.email,
            type.establishment.name,
            type.establishment.phoneNumber,
            type.total,
            type.customer.phoneNumber,
            type.customer.name,
            type.customer.lastNamePaternal,
            type.customer.lastNameMaternal,
            type.customer.address,
            type.customer.latitude,
            type.customer.longitude,
            getOrdersDetail(type.orderDetail),
            type.updateDate,
            type.paymentMethod.customerMessage.orEmpty(),
            type.paymentMethod.description.orEmpty(),
            type.paymentMethod.establishmentMessage.orEmpty(),
            type.paymentMethod.paymentMethodId,
            type.paymentMethod.requestedAmount,
            ammount,
            delivery,
            type.customer.urbanization
        )
    }

    private fun getOrdersDetail(list: List<OrderDetailItem>): List<OrderDetailItemEntity> {
        val listOrdersDetail = mutableListOf<OrderDetailItemEntity>()
        list.forEach {
            listOrdersDetail.add(establishmentDetailItemsEntityMapper.mapFromRemote(it))
        }
        return listOrdersDetail
    }
}