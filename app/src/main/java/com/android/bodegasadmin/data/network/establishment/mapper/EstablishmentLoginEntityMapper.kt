package com.android.bodegasadmin.data.network.establishment.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.establishment.model.EstablishmentLoginBody
import com.android.bodegasadmin.data.network.establishment.model.EstablishmentLoginResponse
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentLoginEntity

class EstablishmentLoginEntityMapper : Mapper<EstablishmentLoginResponse, EstablishmentLoginEntity> {

    override fun mapFromRemote(type: EstablishmentLoginResponse): EstablishmentLoginEntity {
        return EstablishmentLoginEntity(
            type.idEntity,
            type.token
        )
    }
}