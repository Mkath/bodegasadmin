package com.android.bodegasadmin.data.repository.allschedules.model

data class AllScheduleEntity(
    val scheduleId: Int,
    val range: String,
    val type: String,
    val startTime: String ? = "",
    val endTime: String? = ""
)