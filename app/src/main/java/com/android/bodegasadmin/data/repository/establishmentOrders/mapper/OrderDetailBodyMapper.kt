package com.android.bodegasadmin.data.repository.establishmentOrders.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailBodyEntity
import com.android.bodegasadmin.domain.establishmentOrders.OrderDetailBody

class OrderDetailBodyMapper : Mapper<OrderDetailBodyEntity, OrderDetailBody> {

    override fun mapFromEntity(type: OrderDetailBodyEntity): OrderDetailBody {
        return OrderDetailBody(
            type.orderDetailId,
            type.state
        )
    }

    override fun mapToEntity(type: OrderDetailBody): OrderDetailBodyEntity {
        return OrderDetailBodyEntity(
            type.orderDetailId,
            type.state
        )
    }

}