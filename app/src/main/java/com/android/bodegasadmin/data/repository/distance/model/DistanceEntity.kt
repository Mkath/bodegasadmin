package com.android.bodegasadmin.data.repository.distance.model

data class DistanceEntity(
    val distanceInKms: String,
    val numberOfBlocks: Int,
    val travelTimeByCar: String,
    val travelTimeByBike: String,
    val travelTimeByWalking: String
)