package com.android.bodegasadmin.data.network.establishment.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.allPaymentMethods.mapper.AllPaymentMethodsEntityMapper
import com.android.bodegasadmin.data.network.establishment.model.EstablishmentResponse
import com.android.bodegasadmin.data.network.establishment.model.PaymentMethodResponse
import com.android.bodegasadmin.data.network.establishment.model.ScheduleResponse
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.PaymentMethodEntity
import com.android.bodegasadmin.data.repository.establishment.model.ScheduleEntity


class EstablishmentEntityMapper(
    private val scheduleEntityMapper: ScheduleEntityMapper,
    private val paymentMethodEntityMapper: PaymentMethodsEntityMapper
) : Mapper<EstablishmentResponse, EstablishmentEntity> {

    override fun mapFromRemote(type: EstablishmentResponse): EstablishmentEntity {
        var shippingList = listOf<ScheduleResponse>()
        if(!type.shippingSchedule.isNullOrEmpty()){
            shippingList = type.shippingSchedule
        }
        return EstablishmentEntity(
            type.establishmentId,
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.delivery,
            type.pathImage,
            getListSchedules(shippingList),
            getListSchedules(type.operationSchedule),
            type.establishmentType.name,
            type.deliveryCharge,
            getListPaymentMethods(type.paymentMethod)

        )
    }

    private fun getListSchedules(list: List<ScheduleResponse>): List<ScheduleEntity>{
       val listSchedule = mutableListOf<ScheduleEntity>()
        list.forEach {
            listSchedule.add(scheduleEntityMapper.mapFromRemote(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethods(list: List<PaymentMethodResponse>): List<PaymentMethodEntity>{
        val listSchedule = mutableListOf<PaymentMethodEntity>()
        if(list.isNotEmpty()){
            list.forEach {
                listSchedule.add(paymentMethodEntityMapper.mapFromRemote(it))
            }
        }
        return listSchedule
    }

}