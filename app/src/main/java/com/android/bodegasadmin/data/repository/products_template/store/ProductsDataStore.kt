package com.android.bodegasadmin.data.repository.products_template.store

import com.android.bodegasadmin.data.repository.products_template.model.ProductsEntity
import com.android.bodegasadmin.domain.util.Resource

interface ProductsDataStore {
    suspend fun getProducts(
        text:String
    ): Resource<List<ProductsEntity>>


    suspend fun addProducts(
        productId:Int, productPrice:String
    ): Resource<Boolean>
}