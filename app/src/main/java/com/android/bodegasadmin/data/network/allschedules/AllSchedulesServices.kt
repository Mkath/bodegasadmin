package com.android.bodegasadmin.data.network.allschedules

import com.android.bodegasadmin.data.network.allschedules.model.AllSchedulesListBody
import com.android.bodegasadmin.data.network.establishment.model.EstablishmentListBody
import com.android.bodegasadmin.data.network.establishment.model.EstablishmentResponse
import com.android.bodegasadmin.data.network.establishment.model.RegisterEstablishmentBodyRemote
import com.android.bodegasadmin.data.network.establishment.model.UserBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AllSchedulesServices {

    @GET("/common/schedules")
    suspend fun getAllSchedules(): Response<AllSchedulesListBody>

}