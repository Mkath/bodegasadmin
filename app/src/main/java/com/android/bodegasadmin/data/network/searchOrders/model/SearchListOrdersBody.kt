package com.android.bodegasadmin.data.network.searchOrders.model


import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchListOrdersBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var data: DataItem? = null
}

class DataItem (
    @SerializedName("ordersTotal")
    val ordersTotal: Int,
    @SerializedName("orders")
    val orders: List<SearchListOrdersBodyResponse>
)
