package com.android.bodegasadmin.data.network.allschedules.model

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AllSchedulesListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var scheduleItem: ScheduleItem? = null
}

class ScheduleItem(
    @SerializedName("schedules")
    @Expose
    var schedule: List<AllSchedulesResponse>
)
