package com.android.bodegasadmin.data.network.establishment.model

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class EstablishmentListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var establishmentItem: EstablishmentItem? = null
}

class EstablishmentItem(
    @SerializedName("establishment")
    @Expose
    var establishment: EstablishmentResponse
)
