package com.android.bodegasadmin.data.repository.establishmentOrders.mapper

import com.android.bodegasadmin.domain.establishmentOrders.OrderDetails
import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailItemEntity

class EstablishmentDetailItemMapper : Mapper<OrderDetailItemEntity, OrderDetails> {

    override fun mapFromEntity(type: OrderDetailItemEntity): OrderDetails {
        return OrderDetails(
        type.orderDetailOrderDetailId,
        type.orderDetailUnitMeasure,
        type.orderDetailQuantity,
        type.orderDetailPrice,
        type.orderDetailStatus,
        type.orderDetailObservation,
        type.orderDetailSubTotal,
        type.storeProductStoreProductId,
        type.storeProductPrice,
        type.storeProductStatus,
        type.productTemplateProductTemplateId,
        type.productTemplateCode,
        type.productTemplateName,
        type.productTemplateDescription,
        type.productTemplateUnitMeasure,
        type.productTemplateStatus,
        type.productTemplatePathImage
        )
    }

    override fun mapToEntity(type: OrderDetails): OrderDetailItemEntity {
        return OrderDetailItemEntity(
            type.orderDetailOrderDetailId,
            type.orderDetailUnitMeasure,
            type.orderDetailQuantity,
            type.orderDetailPrice,
            type.orderDetailStatus,
            type.orderDetailObservation,
            type.orderDetailSubTotal,
            type.storeProductStoreProductId,
            type.storeProductPrice,
            type.storeProductStatus,
            type.productTemplateProductTemplateId,
            type.productTemplateCode,
            type.productTemplateName,
            type.productTemplateDescription,
            type.productTemplateUnitMeasure,
            type.productTemplateStatus,
            type.productTemplatePathImage
        )
    }

}