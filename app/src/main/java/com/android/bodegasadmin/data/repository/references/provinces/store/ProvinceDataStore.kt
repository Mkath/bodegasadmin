package com.android.bodegasadmin.data.repository.references.provinces.store

import com.android.bodegasadmin.data.repository.references.provinces.model.ProvinceEntity
import com.android.bodegasadmin.domain.util.Resource


interface ProvinceDataStore {
    suspend fun getProvinces(
         id: String,
         name: String,
         department_id: String
    ): Resource<List<ProvinceEntity>>
}

