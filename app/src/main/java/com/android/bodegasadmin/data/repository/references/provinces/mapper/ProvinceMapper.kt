package com.android.bodegasadmin.data.repository.references.provinces.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.references.provinces.model.ProvinceEntity
import com.android.bodegasadmin.domain.references.province.Province

class ProvinceMapper: Mapper<ProvinceEntity, Province> {
    override fun mapFromEntity(type: ProvinceEntity): Province {
        return Province(type.id, type.name, type.department_id)
    }

    override fun mapToEntity(type: Province): ProvinceEntity {
        return ProvinceEntity(type.id, type.name, type.department_id)
    }
}