package com.android.bodegasadmin.data.repository.establishment.model

data class PaymentMethodEntity(
    val paymentMethodId: Int,
    val requestedAmount: Boolean,
    val description: String,
    val customerMessage: String
)