package com.android.bodegasadmin.data.network.allPaymentMethods

import com.android.bodegasadmin.data.network.allPaymentMethods.mapper.AllPaymentMethodsEntityMapper
import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity
import com.android.bodegasadmin.data.repository.allPaymentMethods.store.remote.AllPaymentMethodsRemote

import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext

class AllPaymentMethodsRemoteImpl constructor(
    private val allPaymentMethodsServices: AllPaymentMethodsServices,
    private val allPaymentMethodsEntityMapper: AllPaymentMethodsEntityMapper
) : AllPaymentMethodsRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethodsEntity>>
    {
        return coroutineScope {
            try {
                val response = allPaymentMethodsServices.getAllPaymentMethods()
                if (response.isSuccessful) {
                    if(response.body()!!.success){
                        val allPaymentMethodsResponse = response.body()!!.paymentMethodsItem!!.paymentMethods
                        val allPaymentMethodsEntity = mutableListOf<AllPaymentMethodsEntity>()
                        allPaymentMethodsResponse.forEach {
                            allPaymentMethodsEntity.add(allPaymentMethodsEntityMapper.mapFromRemote(it))
                        }
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            allPaymentMethodsEntity,
                            response.message()
                        )
                    }
                    else {
                        val message = response.body()!!.message
                        return@coroutineScope Resource(
                            Status.ERROR,
                            mutableListOf<AllPaymentMethodsEntity>(),
                            message
                        )
                    }
                } else {
                    return@coroutineScope Resource(
                        Status.ERROR,
                        mutableListOf<AllPaymentMethodsEntity>(),
                        response.message()
                    )
                  }
            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    mutableListOf<AllPaymentMethodsEntity>(),
                    "500"
                )
            } catch (e: Throwable) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    mutableListOf<AllPaymentMethodsEntity>(),
                    e.message
                )
            }
        }
    }
}