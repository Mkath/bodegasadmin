package com.android.bodegasadmin.data.repository.references.districts.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.references.districts.model.DistrictEntity
import com.android.bodegasadmin.domain.references.district.District


class DistrictMapper: Mapper<DistrictEntity, District> {
    override fun mapFromEntity(type: DistrictEntity): District {
        return District(type.id, type.name,type.province_id, type.department_id)
    }

    override fun mapToEntity(type: District): DistrictEntity {
        return DistrictEntity(type.id, type.name,type.province_id, type.department_id)
    }
}