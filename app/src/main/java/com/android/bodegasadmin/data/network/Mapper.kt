package com.android.bodegasadmin.data.network

interface Mapper<in M, out E> {

    fun mapFromRemote(type: M): E

}