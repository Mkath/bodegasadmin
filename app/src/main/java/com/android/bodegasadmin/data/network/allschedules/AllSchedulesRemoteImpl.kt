package com.android.bodegasadmin.data.network.allschedules

import com.android.bodegasadmin.data.network.allschedules.mapper.AllSchedulesEntityMapper
import com.android.bodegasadmin.data.network.establishment.mapper.EstablishmentEntityMapper
import com.android.bodegasadmin.data.network.establishment.mapper.RegisterEstablishmentEntityMapper
import com.android.bodegasadmin.data.network.establishment.model.UserBody
import com.android.bodegasadmin.data.repository.allschedules.model.AllScheduleEntity
import com.android.bodegasadmin.data.repository.allschedules.store.remote.AllSchedulesRemote
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity
import com.android.bodegasadmin.data.repository.establishment.store.remote.EstablishmentRemote
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.*
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext


class AllSchedulesRemoteImpl constructor(
    private val allSchedulesServices: AllSchedulesServices,
    private val allSchedulesEntityMapper: AllSchedulesEntityMapper
) : AllSchedulesRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getAllSchedules(type: String): Resource<List<AllScheduleEntity>>
    {
        return coroutineScope {
            try {
                val response = allSchedulesServices.getAllSchedules()
                if (response.isSuccessful) {
                    if(response.body()!!.success){
                        val allSchedulesResponse = response.body()!!.scheduleItem!!.schedule
                        val allSchedulesEntity = mutableListOf<AllScheduleEntity>()
                        allSchedulesResponse.forEach {
                            if(it.type == type){
                                allSchedulesEntity.add(allSchedulesEntityMapper.mapFromRemote(it))
                            }
                        }
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            allSchedulesEntity,
                            response.message()
                        )
                    }else{
                        val message = response.body()!!.message
                        return@coroutineScope Resource(
                            Status.ERROR,
                            mutableListOf<AllScheduleEntity>(),
                            message
                        )
                    }
                } else {
                    return@coroutineScope Resource(
                        Status.ERROR,
                        mutableListOf<AllScheduleEntity>(),
                        response.message()
                    )
                }
            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    mutableListOf<AllScheduleEntity>(),
                    "500"
                )
            } catch (e: Throwable) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    mutableListOf<AllScheduleEntity>(),
                    e.message
                )
            }
        }
    }
}