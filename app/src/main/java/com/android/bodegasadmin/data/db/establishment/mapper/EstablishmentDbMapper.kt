package com.android.bodegasadmin.data.db.establishment.mapper

import com.android.bodegasadmin.data.db.Mapper
import com.android.bodegasadmin.data.db.establishment.model.EstablishmentDb
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity

class EstablishmentDbMapper : Mapper<EstablishmentDb, EstablishmentEntity> {

    override fun mapFromDb(type: EstablishmentDb): EstablishmentEntity {
        return EstablishmentEntity(
            type.establishmentId,
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.delivery,
            type.pathImage,
            mutableListOf(),
            mutableListOf(),
            type.establishmentType,
            type.deliveryCharge,
            mutableListOf()
        )
    }

    override fun mapToDb(type: EstablishmentEntity): EstablishmentDb {
        return EstablishmentDb(
            type.establishmentId,
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.delivery,
            type.pathImage,
            type.establishmentType,
            type.deliveryCharge
        )
    }
}