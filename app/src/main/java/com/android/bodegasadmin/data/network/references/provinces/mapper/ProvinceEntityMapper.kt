package com.android.bodegasadmin.data.network.references.provinces.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.references.provinces.model.ProvinceResponse
import com.android.bodegasadmin.data.repository.references.provinces.model.ProvinceEntity

class ProvinceEntityMapper : Mapper<ProvinceResponse, ProvinceEntity> {
    override fun mapFromRemote(type: ProvinceResponse): ProvinceEntity {
        return ProvinceEntity(
            type.id,
            type.name,
            type.department_id
        )
    }
}