package com.android.bodegasadmin.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class ResponseBodyError {

    @SerializedName("state")
    @Expose
    val state: String? = null

    @SerializedName("message")
    @Expose
    val message: String? = null

    @SerializedName("fields")
    @Expose
    val fields = listOf<FieldsItem>()

}

class FieldsItem{
    @SerializedName("message")
    @Expose
    val message: String? = null
    @SerializedName("field-name")
    @Expose
    val fieldName: String? = ""
}
