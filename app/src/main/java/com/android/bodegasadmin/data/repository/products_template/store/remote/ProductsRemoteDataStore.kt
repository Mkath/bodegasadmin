package com.android.bodegasadmin.data.repository.products_template.store.remote

import com.android.bodegasadmin.data.repository.products_template.model.ProductsEntity
import com.android.bodegasadmin.data.repository.products_template.store.ProductsDataStore
import com.android.bodegasadmin.domain.util.Resource


class ProductsRemoteDataStore constructor(private val productsRemote: ProductsRemote) :
    ProductsDataStore {

    override suspend fun getProducts(text:String
    ): Resource<List<ProductsEntity>> {
        return productsRemote.getProducts(text)
    }

    override suspend fun addProducts(productId:Int, productPrice:String): Resource<Boolean> {
        return productsRemote.addProducts(productId, productPrice)
    }


}