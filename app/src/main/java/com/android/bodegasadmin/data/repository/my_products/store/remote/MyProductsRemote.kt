package com.android.bodegasadmin.data.repository.my_products.store.remote

import com.android.bodegasadmin.data.repository.my_products.model.MyProductsEntity
import com.android.bodegasadmin.domain.util.Resource


interface MyProductsRemote {

    suspend fun getMyProducts(
        searchText:String
    ): Resource<List<MyProductsEntity>>

    suspend fun updateMyProduct(
        storeProductId:Int,
        price:Double,
        stock:String
    ): Resource<Boolean>

}