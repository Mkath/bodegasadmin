package com.android.bodegasadmin.data.network.allPaymentMethods.model

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AllPaymentMethodsListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var paymentMethodsItem: ScheduleItem? = null
}

class ScheduleItem(
    @SerializedName("paymentMethods")
    @Expose
    var paymentMethods: List<AllPaymentMethodsResponse>
)