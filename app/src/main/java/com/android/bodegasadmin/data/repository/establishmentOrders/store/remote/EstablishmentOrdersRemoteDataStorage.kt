package com.android.bodegasadmin.data.repository.establishmentOrders.store.remote

import com.android.bodegasadmin.data.repository.establishmentOrders.model.EstablishmentOrdersEntity
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailBodyEntity
import com.android.bodegasadmin.data.repository.establishmentOrders.store.EstablishmentOrdersDataStore
import com.android.bodegasadmin.domain.util.Resource

class EstablishmentOrdersRemoteDataStorage constructor(private val establishmentOrderRemote: EstablishmentOrdersRemote)  :
    EstablishmentOrdersDataStore {
    override suspend fun getOrdersListByEstablishmentAndState(establishmentId: Int,
                                                       orderState: String): Resource<List<EstablishmentOrdersEntity>> {
        return establishmentOrderRemote.getOrdersListByEstablishmentAndState(establishmentId, orderState)
    }

    override suspend fun updateStateOrder(
        deliveryCharge: Double,
        orderId: Int,
        state: String,
        list: List<OrderDetailBodyEntity>
    ): Resource<Boolean> {
        return establishmentOrderRemote.updateStateOrder(deliveryCharge,orderId,state,list)
    }

}
