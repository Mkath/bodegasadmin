package com.android.bodegasadmin.data.repository.references.departments.store.remote

import com.android.bodegasadmin.data.repository.references.departments.model.DepartmentEntity
import com.android.bodegasadmin.data.repository.references.departments.store.DepartmentDataStore
import com.android.bodegasadmin.domain.util.Resource

class DepartmentRemoteDataStorage constructor(private val departmentRemote: DepartmentRemote)  :
    DepartmentDataStore {
    override suspend fun getDepartments(
    ): Resource<List<DepartmentEntity>> {
        return departmentRemote.getDepartments()
    }
}

