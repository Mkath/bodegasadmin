package com.android.bodegasadmin.data.network.allPaymentMethods.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.allPaymentMethods.model.AllPaymentMethodsResponse
import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity

class AllPaymentMethodsEntityMapper : Mapper<AllPaymentMethodsResponse, AllPaymentMethodsEntity> {

    override fun mapFromRemote(type: AllPaymentMethodsResponse): AllPaymentMethodsEntity {
        return AllPaymentMethodsEntity(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}