package com.android.bodegasadmin.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.bodegasadmin.data.db.establishment.dao.EstablishmentDao
import com.android.bodegasadmin.data.db.establishment.model.EstablishmentDb

@Database(
    entities = [EstablishmentDb::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun establishmentDao(): EstablishmentDao
   // abstract fun saveOrderDao(): SaveOrderDao

}
