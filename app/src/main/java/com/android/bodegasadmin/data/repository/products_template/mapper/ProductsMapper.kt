package com.android.bodegasadmin.data.repository.products_template.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.products_template.model.ProductsEntity
import com.android.bodegasadmin.domain.products_template.Products


class ProductsMapper : Mapper<ProductsEntity, Products> {

    override fun mapFromEntity(type: ProductsEntity): Products {
        return Products(
            type.productTemplateId,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            type.imageProduct,
            type.tag
        )
    }

    override fun mapToEntity(type: Products): ProductsEntity {
        return ProductsEntity(
            type.productTemplateId,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            type.imageProduct,
            type.tag
        )
    }

}