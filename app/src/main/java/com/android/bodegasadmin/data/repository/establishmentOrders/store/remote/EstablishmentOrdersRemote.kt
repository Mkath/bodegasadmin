package com.android.bodegasadmin.data.repository.establishmentOrders.store.remote

import com.android.bodegasadmin.data.repository.establishmentOrders.model.EstablishmentOrdersEntity
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailBodyEntity
import com.android.bodegasadmin.domain.util.Resource

interface EstablishmentOrdersRemote {
    suspend fun getOrdersListByEstablishmentAndState (
        establishmentId: Int,
        orderState: String
    ): Resource<List<EstablishmentOrdersEntity>>


    suspend fun updateStateOrder(
        deliveryCharge: Double,
        orderId: Int,
        state:String,
        list: List<OrderDetailBodyEntity>
    ): Resource<Boolean>
}