package com.android.bodegasadmin.data.repository.searchOrders.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersDetailItemEntity
import com.android.bodegasadmin.domain.searchOrders.SearchOrderDetails

class SearchDetailItemMapper : Mapper<SearchOrdersDetailItemEntity, SearchOrderDetails> {

    override fun mapFromEntity(type: SearchOrdersDetailItemEntity): SearchOrderDetails {
        return SearchOrderDetails(
            type.orderDetailOrderDetailId,
            type.orderDetailUnitMeasure,
            type.orderDetailQuantity,
            type.orderDetailPrice,
            type.orderDetailStatus,
            type.orderDetailObservation,
            type.orderDetailSubTotal,
            type.storeProductStoreProductId,
            type.storeProductPrice,
            type.storeProductStatus,
            type.productTemplateProductTemplateId,
            type.productTemplateCode,
            type.productTemplateName,
            type.productTemplateDescription,
            type.productTemplateUnitMeasure,
            type.productTemplateStatus,
            type.productTemplatePathImage
        )
    }

    override fun mapToEntity(type: SearchOrderDetails): SearchOrdersDetailItemEntity {
        return SearchOrdersDetailItemEntity(
            type.orderDetailOrderDetailId,
            type.orderDetailUnitMeasure,
            type.orderDetailQuantity,
            type.orderDetailPrice,
            type.orderDetailStatus,
            type.orderDetailObservation,
            type.orderDetailSubTotal,
            type.storeProductStoreProductId,
            type.storeProductPrice,
            type.storeProductStatus,
            type.productTemplateProductTemplateId,
            type.productTemplateCode,
            type.productTemplateName,
            type.productTemplateDescription,
            type.productTemplateUnitMeasure,
            type.productTemplateStatus,
            type.productTemplatePathImage
        )
    }

}