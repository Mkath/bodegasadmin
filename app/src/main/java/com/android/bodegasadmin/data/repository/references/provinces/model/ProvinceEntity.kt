package com.android.bodegasadmin.data.repository.references.provinces.model

class ProvinceEntity (
    val id: String,
    val name: String,
    val department_id: String
)