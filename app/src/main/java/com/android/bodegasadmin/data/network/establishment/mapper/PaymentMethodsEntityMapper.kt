package com.android.bodegasadmin.data.network.establishment.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.establishment.model.PaymentMethodResponse
import com.android.bodegasadmin.data.repository.establishment.model.PaymentMethodEntity

class PaymentMethodsEntityMapper : Mapper<PaymentMethodResponse, PaymentMethodEntity> {

    override fun mapFromRemote(type: PaymentMethodResponse): PaymentMethodEntity {
        return PaymentMethodEntity(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}