package com.android.bodegasadmin.data.network.my_products

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.android.bodegasadmin.data.network.my_products.model.MyProductsListBody
import com.android.bodegasadmin.data.network.my_products.model.UpdateProductBody
import retrofit2.Response
import retrofit2.http.*

interface MyProductsServices {

    @GET("/establishments/{establishment-id}/products")
    suspend fun getMyProducts(
        @Path("establishment-id") establishmentId: Int,
        @Query("text") text: String,
        @Query("sortBy") sortBy: String,
        @Query("page") page: Int,
        @Query("size") size: Int
    ): Response<MyProductsListBody>


    @PUT("/establishments/{establishment-id}/storeproduct/{store-product-id}")
    suspend fun updateMyProducts(
        @Path("establishment-id") establishmentId: Int,
        @Path("store-product-id") storeProductId: Int,
        @Body updateProductBody: UpdateProductBody
    ): Response<ResponseBodySuccess>


}
