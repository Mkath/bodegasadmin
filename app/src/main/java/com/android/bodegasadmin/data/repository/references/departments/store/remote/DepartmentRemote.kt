package com.android.bodegasadmin.data.repository.references.departments.store.remote

import com.android.bodegasadmin.data.repository.references.departments.model.DepartmentEntity
import com.android.bodegasadmin.domain.util.Resource


interface DepartmentRemote {
    suspend fun getDepartments (
    ): Resource<List<DepartmentEntity>>
}