package com.android.bodegasadmin.data.network.references.departments.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.references.departments.model.DepartmentResponse
import com.android.bodegasadmin.data.repository.references.departments.model.DepartmentEntity

class DepartmentEntityMapper : Mapper<DepartmentResponse, DepartmentEntity> {
    override fun mapFromRemote(type: DepartmentResponse): DepartmentEntity {
        return DepartmentEntity(
            type.id,
            type.name
        )
    }
}