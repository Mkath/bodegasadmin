package com.android.bodegasadmin.data.repository.references.districts.store.remote

import com.android.bodegasadmin.data.repository.references.districts.model.DistrictEntity
import com.android.bodegasadmin.data.repository.references.districts.store.DistrictDataStore
import com.android.bodegasadmin.domain.util.Resource

class DistrictRemoteDataStorage constructor(private val districtRemote: DistrictRemote)  :
    DistrictDataStore {
    override suspend fun getDistricts(
        id: String,
        name: String,
        province_id: String,
        department_id: String

    ): Resource<List<DistrictEntity>> {
        return districtRemote.getDistricts( id, name, province_id, department_id)
    }
}

