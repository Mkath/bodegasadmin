package com.android.bodegasadmin.data.network.references.district.model

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DistrictListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var districtListBody: DistrictItem? = null
}

class DistrictItem (
    @SerializedName("districtsTotal")
    val districtsTotal: Int,
    @SerializedName("districts")
    val districts: List<DistrictResponse>
)