package com.android.bodegasadmin.data.network.searchOrders

import com.android.bodegasadmin.data.network.searchOrders.model.SearchListOrdersBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SearchOrdersService {

    @GET ("/establishments/{establishment-id}/orders")
    suspend fun getOrdersListByDni(
        @Path("establishment-id") establishmentid: Int,
        @Query("clientDni") clientDni: String?,
        @Query("clientName") clientName: String?,
        @Query("orderStartDate") orderStartDate: String?,
        @Query("orderEndDate") orderEndDate: String?,
        @Query("order-state") orderState: String?,
        @Query("page") page: String?,
        @Query("size") size: String?,
        @Query("sortBy") sortBy: String?
    ): Response<SearchListOrdersBody>
}
