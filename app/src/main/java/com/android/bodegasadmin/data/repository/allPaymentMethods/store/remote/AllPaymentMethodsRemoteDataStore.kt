package com.android.bodegasadmin.data.repository.allPaymentMethods.store.remote

import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity
import com.android.bodegasadmin.data.repository.allPaymentMethods.store.AllPaymentMethodsDataStore
import com.android.bodegasadmin.domain.util.Resource

class AllPaymentMethodsRemoteDataStore constructor(
    private val allPaymentMethodsRemote: AllPaymentMethodsRemote ) : AllPaymentMethodsDataStore {

    override suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethodsEntity>> {
        return  allPaymentMethodsRemote.getAllPaymentMethods()
    }


}