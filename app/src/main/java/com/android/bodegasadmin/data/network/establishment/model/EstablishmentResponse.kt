package com.android.bodegasadmin.data.network.establishment.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class EstablishmentResponse(
    @SerializedName("establishmentId")
    @Expose
    val establishmentId: Int,
    @SerializedName("address")
    @Expose
    val address: String,
    @SerializedName("businessName")
    @Expose
    val businessName: String,
    @SerializedName("cardNumber")
    @Expose
    val cardNumber: String,
    @SerializedName("cardOperator")
    @Expose
    val cardOperator: String,
    @SerializedName("departament")
    @Expose
    val departament: String,
    @SerializedName("district")
    @Expose
    val district: String,
    @SerializedName("dni")
    @Expose
    val dni: String,
    @SerializedName("email")
    @Expose
    val email: String,
    @SerializedName("lastNameMaternal")
    @Expose
    val lastNameMaternal: String,
    @SerializedName("lastNamePaternal")
    @Expose
    val lastNamePaternal: String,
    @SerializedName("latitude")
    @Expose
    val latitude: Double,
    @SerializedName("longitude")
    @Expose
    val longitude: Double,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("phoneNumber")
    @Expose
    val phoneNumber: String,
    @SerializedName("province")
    @Expose
    val province: String,
    @SerializedName("ruc")
    @Expose
    val ruc: String,
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("urbanization")
    @Expose
    val urbanization: String,
    @SerializedName("delivery")
    @Expose
    val delivery: String,
    @SerializedName("pathImage")
    @Expose
    val pathImage: String? = "",
    @SerializedName("shippingSchedule")
    @Expose
    val shippingSchedule: List<ScheduleResponse> ? = null,
    @SerializedName("operationSchedule")
    @Expose
    val operationSchedule: List<ScheduleResponse>,
    @SerializedName("establishmentType")
    @Expose
    val establishmentType: EstablishmentTypeResponse,
    @SerializedName("deliveryCharge")
    @Expose
    val deliveryCharge: Boolean,
    @SerializedName("paymentMethods")
    @Expose
    val paymentMethod: List<PaymentMethodResponse>
)

class EstablishmentTypeResponse(
    @SerializedName("name")
    @Expose
    var name: String
)