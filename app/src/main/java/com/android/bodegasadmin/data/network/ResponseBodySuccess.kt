package com.android.bodegasadmin.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class ResponseBodySuccess {
    @SerializedName("message")
    @Expose
    val message: String? = null
    @SerializedName("success")
    @Expose
    val success: Boolean = true
}