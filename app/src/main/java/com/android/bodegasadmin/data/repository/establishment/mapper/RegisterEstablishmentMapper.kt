package com.android.bodegasadmin.data.repository.establishment.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity
import com.android.bodegasadmin.domain.establishment.register.RegisterEstablishmentBody

class RegisterEstablishmentMapper : Mapper<RegisterEstablishmentBodyEntity, RegisterEstablishmentBody> {
    override fun mapFromEntity(type: RegisterEstablishmentBodyEntity): RegisterEstablishmentBody {
        return RegisterEstablishmentBody(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.delivery,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.establishmentType,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.operationSchedule,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.shippingSchedule,
            type.urbanization,
            type.deliveryCharge,
            type.paymentMethod
        )
    }

    override fun mapToEntity(type: RegisterEstablishmentBody): RegisterEstablishmentBodyEntity {
        return RegisterEstablishmentBodyEntity(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.delivery,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.establishmentType,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.operationSchedule,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.shippingSchedule,
            type.urbanization,
            type.deliveryCharge,
            type.paymentMethod
        )
    }

}