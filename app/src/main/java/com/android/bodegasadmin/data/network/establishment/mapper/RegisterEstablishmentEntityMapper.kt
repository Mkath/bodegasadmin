package com.android.bodegasadmin.data.network.establishment.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.establishment.model.RegisterEstablishmentBodyRemote
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity

class RegisterEstablishmentEntityMapper : Mapper<RegisterEstablishmentBodyEntity, RegisterEstablishmentBodyRemote> {

    override fun mapFromRemote(type: RegisterEstablishmentBodyEntity): RegisterEstablishmentBodyRemote {
        return RegisterEstablishmentBodyRemote(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.dni,
            type.delivery,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.establishmentType,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.operationSchedule,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.shippingSchedule,
            type.urbanization,
            type.deliveryCharge,
            type.paymentMethod
        )
    }

}
