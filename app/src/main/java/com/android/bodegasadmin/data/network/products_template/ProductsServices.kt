package com.android.bodegasadmin.data.network.products_template

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.android.bodegasadmin.data.network.products_template.model.ProductsBodySend
import com.android.bodegasadmin.data.network.products_template.model.ProductsListBody
import retrofit2.Response
import retrofit2.http.*

interface ProductsServices {

    @GET("/products")
    suspend fun getProducts(
        @Query("text") text:String,
        @Query("sortBy") sortBy: String
    ): Response<ProductsListBody>

    @POST("/establishments/{establishment-id}/products")
    suspend fun addProducts(
        @Path("establishment-id") establishmentId:Int,
        @Body body: ProductsBodySend
    ): Response<ResponseBodySuccess>

}

