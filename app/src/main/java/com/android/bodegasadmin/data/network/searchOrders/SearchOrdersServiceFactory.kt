package com.android.bodegasadmin.data.network.searchOrders

import android.content.Context
import com.android.bodegasadmin.BuildConfig
import com.android.bodegasadmin.data.PreferencesManager
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object SearchOrdersServiceFactory {
    fun makeService(isDebug: Boolean, context: Context): SearchOrdersService {
        val okHttpClient =
            makeOkHttpClient(
                makeLoggingInterceptor(
                    isDebug
                ), context
            )
        return makeService(
            okHttpClient,
            makeGson()
        )
    }

    private fun makeService(okHttpClient: OkHttpClient, gson: Gson): SearchOrdersService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit.create(SearchOrdersService::class.java)
    }

    private fun makeOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor, context: Context): OkHttpClient {
        val okHttpBuilder = OkHttpClient.Builder()

        return setHeader(okHttpBuilder, context)
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()
    }

    private fun makeGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }

    private fun makeLoggingInterceptor(isDebug: Boolean): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
        return logging
    }

    private fun setHeader(
        okHttpBuilder: OkHttpClient.Builder,
        context: Context
    ): OkHttpClient.Builder {

        okHttpBuilder.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .header(
                    "Authorization",
                    "Bearer " + PreferencesManager.getInstance().getEstablishmentLoginTokenSelected().token
                )
            val request = requestBuilder.build()
            chain.proceed(request)
        }
        return okHttpBuilder
    }

}