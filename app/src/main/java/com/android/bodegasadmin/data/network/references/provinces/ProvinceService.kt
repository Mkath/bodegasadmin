package com.android.bodegasadmin.data.network.references.provinces

import com.android.bodegasadmin.data.network.references.provinces.model.ProvinceListBody
import retrofit2.Response
import retrofit2.http.GET

interface ProvinceService {
    @GET("/provinces")
    suspend fun getProvinces() : Response<ProvinceListBody>
}
