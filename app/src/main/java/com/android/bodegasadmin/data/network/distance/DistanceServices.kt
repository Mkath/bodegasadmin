package com.android.bodegasadmin.data.network.distance

import com.android.bodegasadmin.data.network.allPaymentMethods.model.AllPaymentMethodsListBody
import com.android.bodegasadmin.data.network.distance.model.DistanceBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface DistanceServices {

    @GET("/distance")
    suspend fun getDistance(
        @Query("establishmentLatitude") establishmentLatitude: Double,
        @Query("establishmentLongitude") establishmentLongitude: Double,
        @Query("customerLatitude") customerLatitude: Double,
        @Query("customerLongitude") customerLongitude: Double
    ): Response<DistanceBody>
}