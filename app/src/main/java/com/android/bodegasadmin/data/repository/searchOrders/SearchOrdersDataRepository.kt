package com.android.bodegasadmin.data.repository.searchOrders

import android.util.Log
import com.android.bodegasadmin.data.repository.searchOrders.mapper.SearchOrdersMapper
import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersEntity
import com.android.bodegasadmin.data.repository.searchOrders.store.remote.SearchOrdersRemoteDataStorage
import com.android.bodegasadmin.domain.searchOrders.SearchOrderRepository
import com.android.bodegasadmin.domain.searchOrders.SearchEstablishmentOrders
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class SearchOrdersDataRepository (
    private val searchOrdersRemoteDataStore: SearchOrdersRemoteDataStorage,
    private val searchOrdersMapper: SearchOrdersMapper
) : SearchOrderRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

        override suspend fun getOrdersListByDni(establishmentId: Int,
                                                clientDni: String,
                                                clientName: String,
                                                orderStartDate: String,
                                                orderEndDate: String,
                                                orderState: String,
                                                page: String,
                                                size: String,
                                                sortBy: String): Resource<List<SearchEstablishmentOrders>> {
            return coroutineScope {
            val searchOrdersList = searchOrdersRemoteDataStore.getOrdersListByDni(establishmentId,
                                                                                            clientDni,
                                                                                            clientName,
                                                                                            orderStartDate,
                                                                                            orderEndDate,
                                                                                            orderState,
                                                                                            page,
                                                                                            size,
                                                                                            sortBy)

            var resource : Resource<List<SearchEstablishmentOrders>> = Resource(searchOrdersList.status, mutableListOf(), searchOrdersList.message)
            if(searchOrdersList.status == Status.SUCCESS)  {
                val mutableListcustomerOrders = mapClassList(searchOrdersList.data)
                resource = Resource(Status.SUCCESS, mutableListcustomerOrders, searchOrdersList.message)
            }
            else {
                if(searchOrdersList.status == Status.ERROR) {
                    resource = Resource(searchOrdersList.status, mutableListOf(), searchOrdersList.message)
                }
            }
            return@coroutineScope resource
        }
    }

    private fun mapClassList(searchOrdersEntityList: List<SearchOrdersEntity>) : List<SearchEstablishmentOrders> {
        val searchOrdersList = mutableListOf<SearchEstablishmentOrders>()
        searchOrdersEntityList.forEach{
            searchOrdersList.add(searchOrdersMapper.mapFromEntity(it))
        }
        return searchOrdersList
    }
}