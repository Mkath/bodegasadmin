package com.android.bodegasadmin.data.network.searchOrders.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.establishmentOrders.model.OrderDetailItem
import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersDetailItemEntity

class SearchDetailItemEntityMapper : Mapper<OrderDetailItem, SearchOrdersDetailItemEntity> {
    override fun mapFromRemote(type: OrderDetailItem): SearchOrdersDetailItemEntity {
        return SearchOrdersDetailItemEntity(
            type.orderDetailId.toString(),
            type.unitMeasure,
            type.quantity.toString(),
            type.price.toString(),
            type.status,
            type.observation,
            type.subTotal.toString(),
            type.storeProduct.storeProductId.toString(),
            type.storeProduct.price.toString(),
            type.storeProduct.status,
            type.storeProduct.productTemplate.productTemplateId.toString(),
            type.storeProduct.productTemplate.code,
            type.storeProduct.productTemplate.name,
            type.storeProduct.productTemplate.description,
            type.storeProduct.productTemplate.unitMeasure,
            type.storeProduct.productTemplate.status,
            type.storeProduct.productTemplate.pathImage
        )
    }
}