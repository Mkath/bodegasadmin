package com.android.bodegasadmin.data.repository.products_template.store.remote

import com.android.bodegasadmin.data.repository.products_template.model.ProductsEntity
import com.android.bodegasadmin.domain.util.Resource


interface ProductsRemote {

    suspend fun getProducts(
        text:String
    ): Resource<List<ProductsEntity>>


    suspend fun addProducts(
        productId:Int, productPrice:String
    ): Resource<Boolean>

}