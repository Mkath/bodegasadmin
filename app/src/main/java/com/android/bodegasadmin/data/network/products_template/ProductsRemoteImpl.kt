package com.android.bodegasadmin.data.network.products_template

import android.content.Context
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.data.network.products_template.mapper.ProductsEntityMapper
import com.android.bodegasadmin.data.network.products_template.model.ProductBodyItem
import com.android.bodegasadmin.data.network.products_template.model.ProductsBodySend
import com.android.bodegasadmin.data.network.products_template.model.ProductsItem
import com.android.bodegasadmin.data.repository.products_template.model.ProductsEntity
import com.android.bodegasadmin.data.repository.products_template.store.remote.ProductsRemote
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import kotlinx.coroutines.*
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext


class ProductsRemoteImpl constructor(
    private val productsServices: ProductsServices,
    private val productsEntityMapper: ProductsEntityMapper,
    private val context: Context
) : ProductsRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getProducts(text:String): Resource<List<ProductsEntity>> {
        return coroutineScope {
            try {
                val result = productsServices.getProducts(text,
                    "description")

                if (result.isSuccessful) {
                    if(result.code() == 200){
                        result.body()?.let {
                            val productsEntityList = mapList(result.body()!!.productsListBody)
                            productsEntityList?.let {
                                return@coroutineScope Resource(Status.SUCCESS, productsEntityList, "")
                            }
                        }
                    }else if (result.code() == 204){
                        return@coroutineScope Resource(Status.SUCCESS,  listOf<ProductsEntity>(), "")
                    }

                }
                return@coroutineScope Resource(Status.ERROR, listOf<ProductsEntity>(), "")

            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<ProductsEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProductsEntity>(), "")
            }
        }
    }

    override suspend fun addProducts(productId:Int, productPrice:String): Resource<Boolean> {
        return coroutineScope {
            try {
                val productBody = ProductBodyItem(productPrice, productId)
                val productsBodySend = ProductsBodySend(mutableListOf(productBody))
                val result = productsServices.addProducts(
                    PreferencesManager.getInstance().getEstablishmentSelected().storeId, productsBodySend)
                if (result.isSuccessful) {
                    if(result.body()!!.success){
                        return@coroutineScope Resource(Status.SUCCESS, true, "")
                    }else{
                        return@coroutineScope Resource(Status.ERROR,  false, result.body()!!.message)
                    }

                }
                return@coroutineScope Resource(Status.ERROR, false, "")

            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    false,
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, false, "")
            }
        }
    }

    private fun mapList(productsListBody: ProductsItem?): List<ProductsEntity>? {
        val productsList = ArrayList<ProductsEntity>()
        productsListBody?.listProducts?.forEach {
            productsList.add(productsEntityMapper.mapFromRemote(it))
        }
        return productsList
    }


}