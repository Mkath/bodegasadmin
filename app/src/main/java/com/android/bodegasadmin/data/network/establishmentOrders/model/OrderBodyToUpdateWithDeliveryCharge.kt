package com.android.bodegasadmin.data.network.establishmentOrders.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class OrderBodyToUpdateWithDeliveryCharge (
    @SerializedName("deliveryCharge")
    val deliveryCharge: Double,
    @SerializedName("state")
    val state: String,
    @SerializedName("updateUser")
    val updateUser: String,
    @SerializedName("stateOrderDetailList")
    val stateOrderDetailList: List<OrderDetailBodyToSend>
)