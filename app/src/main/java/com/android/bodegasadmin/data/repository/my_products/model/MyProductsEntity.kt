package com.android.bodegasadmin.data.repository.my_products.model

data class MyProductsEntity(
    val storeProductId: Int,
    val price: Double,
    val status: String,
    val code: String,
    val name: String,
    val description: String,
    val unitMeasure: String,
    val stock:String,
    val imageProduct: String ? = ""
)
