package com.android.bodegasadmin.data.network.references.departments.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DepartmentResponse (
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("name")
    @Expose
    val name: String
)