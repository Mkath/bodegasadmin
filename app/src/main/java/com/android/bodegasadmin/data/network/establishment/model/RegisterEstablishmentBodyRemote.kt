package com.android.bodegasadmin.data.network.establishment.model

import com.google.gson.annotations.SerializedName

class RegisterEstablishmentBodyRemote(
    @SerializedName("address")
    val address: String,
    @SerializedName("businessName")
    val businessName: String,
    @SerializedName("cardNumber")
    val cardNumber: String,
    @SerializedName("cardOperator")
    val cardOperator: String,
    @SerializedName("country")
    val country:String = "PE",
    @SerializedName("creationUser")
    val creationUser: String,
    @SerializedName("delivery")
    var delivery: String?,
    @SerializedName("departament")
    val department: String,
    @SerializedName("district")
    val district: String,
    @SerializedName("dni")
    val dni: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("establishmentType")
    val establishmentType: Int,
    @SerializedName("lastNameMaternal")
    val lastNameMaternal: String,
    @SerializedName("lastNamePaternal")
    val lastNamePaternal: String,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double,
    @SerializedName("name")
    val name: String,
    @SerializedName("operationSchedule")
    val operationSchedule: List<Int>,
    @SerializedName("password")
    val password: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("province")
    val province: String,
    @SerializedName("ruc")
    val ruc: String,
    @SerializedName("shippingSchedule")
    var shippingSchedule: List<Int>,
    @SerializedName("urbanization")
    val urbanization: String,
    @SerializedName("deliveryCharge")
    val deliveryCharge: Boolean,
    @SerializedName("paymentMethod")
    val paymentMethod: List<Int>
)