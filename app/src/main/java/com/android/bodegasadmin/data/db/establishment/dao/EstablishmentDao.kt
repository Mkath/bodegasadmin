package com.android.bodegasadmin.data.db.establishment.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.bodegasadmin.data.db.establishment.model.EstablishmentDb

@Dao
abstract class EstablishmentDao {

    @Query(EstablishmentConstants.QUERY_USER)
    abstract suspend fun getEstablishment(): EstablishmentDb

    @Query(EstablishmentConstants.DELETE_USER)
    abstract suspend fun clearEstablishment()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertEstablishment(establishmentDb: EstablishmentDb)

}