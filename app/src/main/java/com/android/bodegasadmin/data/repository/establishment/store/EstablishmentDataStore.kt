package com.android.bodegasadmin.data.repository.establishment.store

import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentEntity
import com.android.bodegasadmin.data.repository.establishment.model.EstablishmentLoginEntity
import com.android.bodegasadmin.data.repository.establishment.model.RegisterEstablishmentBodyEntity
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.presentation.login.ForgetPassDTO
import com.android.bodegasadmin.presentation.profile.ChangePasswordDtoBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentScheduledDTO

interface EstablishmentDataStore {

    suspend fun loginEstablishmentToken(email: String, password: String): Resource<EstablishmentLoginEntity>

    suspend fun loginEstablishment(establishmentId: Int): Resource<EstablishmentEntity>

    suspend fun saveEstablishment(establishmentEntity: EstablishmentEntity)

    suspend fun clearEstablishment()

    suspend fun getEstablishment(): Resource<EstablishmentEntity>

    suspend fun registerEstablishment(
        registerEstablishmentBodyEntity: RegisterEstablishmentBodyEntity
    ): Resource<EstablishmentLoginEntity>

    suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean>

    suspend fun updateEstablishment(customerId: Int, body: UpdateEstablishmentBody): Resource<EstablishmentEntity>

    suspend fun updatePassword( body: ChangePasswordDtoBody): Resource<Boolean>

    suspend fun recoveryPassword( body: ForgetPassDTO): Resource<Boolean>

    suspend fun updateSchedule(establishmentId:Int, body: UpdateEstablishmentScheduledDTO): Resource<Boolean>
}