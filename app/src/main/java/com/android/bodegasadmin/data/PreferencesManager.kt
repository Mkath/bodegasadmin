package com.android.bodegasadmin.data

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.android.bodegasadmin.domain.establishment.update.UpdateDataEstablishment
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentLoginTokenView
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentView
import com.android.bodegasadmin.presentation.establishment.model.RegisterEstablishmentBodyView
import com.android.bodegasadmin.presentation.profile.UpdateEstablishmentBody
import com.google.gson.Gson
import org.koin.core.KoinComponent
import org.koin.core.inject

class PreferencesManager : KoinComponent {

    companion object {
        const val MY_ESTABLISHMENT_LOGIN_TOKEN = "my_establishment_login_token"
        const val MY_ESTABLISHMENT = "my_establishment"
        const val LIST_CURRENT_DELIVERY_SCHEDULES = "list_current_delivery_schedules"
        const val REGISTER_ESTABLISHMENT = "register_establishment"
        const val UPDATE_DATA_ESTABLISHMENT = "update_data_establishment"
        const val IS_LOGIN = "is_login"
        const val USER_TOKEN = "token"

        private var ourInstance: PreferencesManager? = null

        fun getInstance(): PreferencesManager {
            if (ourInstance == null) {
                ourInstance = PreferencesManager()
            }
            return ourInstance as PreferencesManager
        }
    }

    private val application: Application by inject()

    private var preferences: SharedPreferences
    private val editor: SharedPreferences.Editor

    init {
        preferences = application.getSharedPreferences(
            application.applicationContext.packageName,
            Context.MODE_PRIVATE
        )
        editor = preferences.edit()
    }

    fun setIsLogin(isLogin: Boolean) {
        editor.putBoolean(IS_LOGIN, isLogin).apply()
    }

    fun isLogin(): Boolean {
        return preferences.getBoolean(IS_LOGIN, false)
    }

    fun setUserToken(token: String) {
        editor.putString(USER_TOKEN, token).apply()
    }

    fun setListCurrentDeliverySchedules(listCurrentDeliverySchedules: String) {
        val list = preferences.getString(LIST_CURRENT_DELIVERY_SCHEDULES, "");
        editor.putString(LIST_CURRENT_DELIVERY_SCHEDULES, listCurrentDeliverySchedules).apply()
    }

    fun getListCurrentDeliverySchedules():String? {
        val list: String? = preferences.getString(LIST_CURRENT_DELIVERY_SCHEDULES, "");
        return list
    }

    fun closeSession() {
        editor.clear()
        editor.commit() // commit changes
    }

    fun setEstablishmentSelected(establishment: EstablishmentView) {
        val gson = Gson()
        val json = gson.toJson(establishment)
        editor.putString(MY_ESTABLISHMENT, json)
        editor.commit()
    }

    fun getEstablishmentSelected(): EstablishmentView {
        val gson = Gson()
        val json = preferences.getString(MY_ESTABLISHMENT, "");
        var obj = gson.fromJson(json, EstablishmentView::class.java)
        if(obj == null){
            obj = EstablishmentView(0,"","","",0.0,0.0,
                mutableListOf(), mutableListOf(), "" ,"", true, "", "", "", "", "", "", "", "", "","", "","","",true, mutableListOf() )
        }
        return obj
    }

    fun cleanEstablishment(){
        editor.putString(MY_ESTABLISHMENT, "").commit()
    }

    fun saveBodyEstablishment(registerEstablishment: RegisterEstablishmentBodyView) {
        val gson = Gson()
        val json = gson.toJson(registerEstablishment)
        editor.putString(REGISTER_ESTABLISHMENT, json)
        editor.commit()
    }

    fun getBodyEstablishment(): RegisterEstablishmentBodyView {
        val gson = Gson()
        val json = preferences.getString(REGISTER_ESTABLISHMENT, "");
        var obj = gson.fromJson(json, RegisterEstablishmentBodyView::class.java)
        if (obj == null) {
            obj = RegisterEstablishmentBodyView(
                "",
                "",
                "",
                "",
                "",
                "",
                "S",
                "",
                "",
                "",
                "",
                1,
                "",
                "",
                0.0,
                0.0,
                "",
                mutableListOf(),
                "",
                "",
                "",
                "",
                mutableListOf(),
                "",
                false,
                mutableListOf()
            )
        }
        return obj
    }


    fun saveBodyUpdateEstablishment(data: UpdateEstablishmentBody) {
        val gson = Gson()
        val json = gson.toJson(data)
        editor.putString(UPDATE_DATA_ESTABLISHMENT, json)
        editor.commit()
    }

    fun getBodyUpdateEstablishment(): UpdateEstablishmentBody {
        val gson = Gson()
        val json = preferences.getString(UPDATE_DATA_ESTABLISHMENT, "");
        var obj = gson.fromJson(json, UpdateEstablishmentBody::class.java)
        if (obj == null) {
            obj = UpdateEstablishmentBody(
                "",
                "",
                "",
                "",
                "",
                "",
                0,
                "",
                "",
                0.0,
                0.0,
                "",
                "",
                "",
                "",
                "",
                "",
                false,
                mutableListOf()
            )
        }
        return obj
    }

    fun cleanUpdateEstablishment(){
        editor.putString(UPDATE_DATA_ESTABLISHMENT, "").commit()
    }

    fun setEstablishmentLoginTokenSelected(establishment: EstablishmentLoginTokenView) {
        val gson = Gson()
        val json = gson.toJson(establishment)
        editor.putString(MY_ESTABLISHMENT_LOGIN_TOKEN, json)
        editor.commit()
    }

    fun getEstablishmentLoginTokenSelected(): EstablishmentLoginTokenView {
        val gson = Gson()
        val json = preferences.getString(MY_ESTABLISHMENT_LOGIN_TOKEN, "");
        var obj = gson.fromJson(json, EstablishmentLoginTokenView::class.java)
        if(obj == null){
            obj = EstablishmentLoginTokenView("","")
        }
        return obj
    }

}
