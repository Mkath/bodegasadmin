package com.android.bodegasadmin.data.network.allschedules.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AllSchedulesResponse(
    @SerializedName("shippingScheduleId")
    @Expose
    val shippingScheduleId: Int,
    @SerializedName("range")
    @Expose
    val range: String,
    @SerializedName("type")
    @Expose
    val type: String,
    @SerializedName("startTime")
    @Expose
    val startTime: String? = "",
    @SerializedName("endTime")
    @Expose
    val endTime: String? = ""
)