package com.android.bodegasadmin.data.repository.references.provinces.store.remote

import com.android.bodegasadmin.data.repository.references.provinces.model.ProvinceEntity
import com.android.bodegasadmin.data.repository.references.provinces.store.ProvinceDataStore
import com.android.bodegasadmin.domain.util.Resource

class ProvinceRemoteDataStorage constructor(private val provinceRemote: ProvinceRemote)  :
    ProvinceDataStore {
    override suspend fun getProvinces(
        id: String,
        name: String,
        department_id: String

    ): Resource<List<ProvinceEntity>> {
        return provinceRemote.getProvinces(id, name, department_id)
    }
}

