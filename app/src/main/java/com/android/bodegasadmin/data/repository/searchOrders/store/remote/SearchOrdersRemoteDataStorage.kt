package com.android.bodegasadmin.data.repository.searchOrders.store.remote

import com.android.bodegasadmin.data.repository.searchOrders.model.SearchOrdersEntity
import com.android.bodegasadmin.data.repository.searchOrders.store.SearchOrdersDataStore
import com.android.bodegasadmin.domain.util.Resource

class SearchOrdersRemoteDataStorage constructor(private val searchOrderRemote: SearchOrdersRemote)  :
    SearchOrdersDataStore {
    override suspend fun getOrdersListByDni(establishmentId: Int,
                                            clientDni: String,
                                            clientName: String,
                                            orderStartDate: String,
                                            orderEndDate: String,
                                            orderState: String,
                                            page: String,
                                            size: String,
                                            sortBy: String): Resource<List<SearchOrdersEntity>> {
        return searchOrderRemote.getOrdersListByDni(establishmentId,
                                                    clientDni,
                                                    clientName,
                                                    orderStartDate,
                                                    orderEndDate,
                                                    orderState,
                                                    page,
                                                    size,
                                                    sortBy)
    }

}
