package com.android.bodegasadmin.data.repository.allPaymentMethods.store.remote

import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity
import com.android.bodegasadmin.domain.util.Resource

interface AllPaymentMethodsRemote {
    suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethodsEntity>>
}