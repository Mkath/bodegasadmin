package com.android.bodegasadmin.data.network.products_template.model

import com.android.bodegasadmin.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProductsListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var productsListBody: ProductsItem? = null
}

class ProductsItem(
    @SerializedName("total")
    val total: Int,
    @SerializedName("products")
    val listProducts: List<ProductsResponse>
)
