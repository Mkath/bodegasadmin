package com.android.bodegasadmin.data.repository.establishment.mapper

import com.android.bodegasadmin.data.repository.Mapper
import com.android.bodegasadmin.data.repository.establishment.model.ScheduleEntity
import com.android.bodegasadmin.domain.schedule.Schedule


class ScheduleMapper : Mapper<ScheduleEntity, Schedule> {

    override fun mapFromEntity(type: ScheduleEntity): Schedule {
        return Schedule(
            type.range,
            type.startTime,
            type.endTime,
            type.shippingScheduleId
        )
    }

    override fun mapToEntity(type: Schedule): ScheduleEntity {
        return ScheduleEntity(
            type.range,
            type.startTime,
            type.endTime,
            type.shippingScheduleId
        )
    }

}