package com.android.bodegasadmin.data.network.establishmentOrders.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.establishmentOrders.model.OrderDetailBodyToSend
import com.android.bodegasadmin.data.repository.establishmentOrders.model.OrderDetailBodyEntity

class OrderDetailBodyEntityMapper : Mapper<OrderDetailBodyEntity, OrderDetailBodyToSend> {

    override fun mapFromRemote(type: OrderDetailBodyEntity): OrderDetailBodyToSend {
        return OrderDetailBodyToSend(
            type.orderDetailId,
            type.state
        )
    }


}