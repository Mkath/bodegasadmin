package com.android.bodegasadmin.data.repository.distance.store.remote

import com.android.bodegasadmin.data.repository.distance.model.DistanceEntity
import com.android.bodegasadmin.domain.util.Resource

interface DistanceRemote {
    suspend fun getDistance(eLat: Double, eLong: Double, cLat :Double,  cLong:Double): Resource<DistanceEntity>
}