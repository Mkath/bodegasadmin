package com.android.bodegasadmin.data.network.distance.mapper

import com.android.bodegasadmin.data.network.Mapper
import com.android.bodegasadmin.data.network.distance.model.DistanceResponse
import com.android.bodegasadmin.data.repository.distance.model.DistanceEntity

class DistanceEntityMapper : Mapper<DistanceResponse, DistanceEntity> {

    override fun mapFromRemote(type: DistanceResponse): DistanceEntity {
        return DistanceEntity(
            type.distanceInKms,
            type.numberOfBlocks,
            type.travelTimeByCar,
            type.travelTimeByBike,
            type.travelTimeByWalking
        )
    }

}