package com.android.bodegasadmin.data.network.allPaymentMethods

import com.android.bodegasadmin.data.network.allPaymentMethods.model.AllPaymentMethodsListBody
import com.android.bodegasadmin.data.network.allschedules.model.AllSchedulesListBody
import retrofit2.Response
import retrofit2.http.GET

interface AllPaymentMethodsServices {

    @GET("/common/paymentmethods")
    suspend fun getAllPaymentMethods(): Response<AllPaymentMethodsListBody>
}