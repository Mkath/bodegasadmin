package com.android.bodegasadmin.presentation.references

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DepartmentView(
    val id: String,
    val name: String
) : Parcelable
