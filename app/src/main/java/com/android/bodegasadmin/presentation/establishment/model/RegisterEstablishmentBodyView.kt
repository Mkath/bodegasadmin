package com.android.bodegasadmin.presentation.establishment.model

import android.os.Parcelable
import com.android.volley.ExecutorDelivery
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterEstablishmentBodyView(
    var address: String,
    var businessName: String,
    var cardNumber: String,
    var cardOperator: String,
    var country:String = "PE",
    var creationUser: String,
    var delivery: String,
    var department: String,
    var district: String,
    var dni: String,
    var email: String,
    var establishmentType: Int,
    var lastNameMaternal: String,
    var lastNamePaternal: String,
    var latitude: Double,
    var longitude: Double,
    var name: String,
    var operationSchedule : List<Int>,
    var password: String,
    var phoneNumber: String,
    var province: String,
    var ruc: String,
    var shippingSchedule : List<Int>,
    var urbanization: String,
    var deliveryCharge: Boolean,
    var paymentMethod : List<Int>
) : Parcelable