package com.android.bodegasadmin.presentation.ordersHistory.mapper


import com.android.bodegasadmin.domain.establishmentOrders.OrderDetails
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.ordersHistory.model.OrdersItemView


class OrderDetailsViewMapper : Mapper<OrdersItemView, OrderDetails> {
    override fun mapToView(type: OrderDetails): OrdersItemView {
        return OrdersItemView(
            type.orderDetailOrderDetailId,
            type.orderDetailUnitMeasure,
            type.orderDetailQuantity,
            type.orderDetailPrice,
            getStringIsAttended(type.orderDetailStatus),
            type.orderDetailObservation,
            type.orderDetailSubTotal,
            type.storeProductStoreProductId,
            type.storeProductPrice,
            type.storeProductStatus,
            type.productTemplateProductTemplateId,
            type.productTemplateCode,
            type.productTemplateName,
            type.productTemplateDescription,
            type.productTemplateUnitMeasure,
            type.productTemplateStatus,
            type.productTemplatePathImage,
            getIsaAttended(type.orderDetailStatus)
        )
    }

    private fun getIsaAttended(value: String): Boolean{
        return value != "N"
    }

    private fun getStringIsAttended(value: String): String{
        return if(value == "N"){
            "No atendido"
        }else{
            "Atendido"
        }
    }
}
