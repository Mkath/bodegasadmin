package com.android.bodegasadmin.presentation.ordersHistory

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.domain.distance.GetDistance
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.mapper.DistanceViewMapper
import com.android.bodegasadmin.presentation.ordersHistory.model.DistanceView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class DistanceViewModel constructor(
    private val getDistance: GetDistance,
    private val distanceViewMapper: DistanceViewMapper
) : ViewModel(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _distance = MutableLiveData<Resource<DistanceView>>()
    val distance: LiveData<Resource<DistanceView>> = _distance

    fun getDistance(
        eLat: Double, eLong: Double, cLat :Double,  cLong:Double
    ) {
        viewModelScope.launch {
            val distanceView = DistanceView("", 0, "", "", "")
            _distance.value = Resource(Status.LOADING, distanceView,"")

            val distanceResult = getDistance.getDistance(eLat, eLong, cLat, cLong)

            if (distanceResult.status == Status.SUCCESS) {
                val distanceResultData = distanceResult.data
                if (distanceResultData.toString().isNotEmpty()) {
                    val distanceOrdersView =    distanceViewMapper.mapToView(distanceResultData)
                    _distance.value = Resource(Status.SUCCESS, distanceOrdersView, "")
                } else {
                    _distance.value = Resource(Status.SUCCESS, distanceView, "")
                }
            } else {
                _distance.value = Resource(Status.ERROR,distanceView, "")
            }
        }
    }

}