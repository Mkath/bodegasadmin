package com.android.bodegasadmin.presentation.searchOrders.mapper

import com.android.bodegasadmin.domain.searchOrders.SearchOrderDetails
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.searchOrders.model.SearchOrdersItemView

class SearchOrderDetailsViewMapper : Mapper<SearchOrdersItemView, SearchOrderDetails> {
    override fun mapToView(type: SearchOrderDetails): SearchOrdersItemView {
        return SearchOrdersItemView(
            type.orderDetailOrderDetailId,
            type.orderDetailUnitMeasure,
            type.orderDetailQuantity,
            type.orderDetailPrice,
            type.orderDetailStatus,
            type.orderDetailObservation,
            type.orderDetailSubTotal,
            type.storeProductStoreProductId,
            type.storeProductPrice,
            type.storeProductStatus,
            type.productTemplateProductTemplateId,
            type.productTemplateCode,
            type.productTemplateName,
            type.productTemplateDescription,
            type.productTemplateUnitMeasure,
            type.productTemplateStatus,
            type.productTemplatePathImage
        )
    }
}
