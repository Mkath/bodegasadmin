package com.android.bodegasadmin.presentation.myproducts

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegasadmin.R
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.myproducts.adapter.MyProductsAdapter
import com.android.bodegasadmin.presentation.myproducts.adapter.MyProductsViewHolder
import com.android.bodegasadmin.presentation.myproducts.model.MyProductsView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_my_products.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [MyProductsFragment.OnListFragmentInteractionListener] interface.
 */
class MyProductsFragment : Fragment(), MyProductsViewHolder.ViewHolderListener {

    companion object {
        const val TAG = "MyProductsFragment"
        @JvmStatic
        fun newInstance() =
            MyProductsFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    private var listener: OnFragmentInteractionListener? = null
    private val myProductsViewModel: MyProductsViewModel by sharedViewModel()

    private var adapter = MyProductsAdapter(this)
    private lateinit var productList: List<MyProductsView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnAddProducts.setOnClickListener {
            listener!!.replaceProductsTemplateFragment()
        }
        observerUpdateProducts()
        observeListProducts()
        searchBehavior()
        btnComeBackToProducts.setOnClickListener{
            onCleanSearch()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        myProductsViewModel.getMyProducts("")
    }

    private fun searchBehavior() {
        search.queryHint = "Buscar"
        search.findViewById<AutoCompleteTextView>(R.id.search_src_text).threshold = 1

        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                query?.let {
                    myProductsViewModel.getMyProducts(it)
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return true
            }
        })
    }


    private fun observeListProducts() {

        myProductsViewModel.listProducts.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    search.setIconified(true)
                    if (isOpenKeyboard()){
                        hideKeyboard()
                    }
                    listener!!.hideLoading()
                    productList = it.data
                    if (productList.isNotEmpty()) {
                        containerEmptyState.visibility = View.GONE
                        rvProducts.visibility = View.VISIBLE
                        setRecyclerView()

                    } else {
                        containerEmptyState.visibility = View.VISIBLE
                        rvProducts.visibility = View.GONE
                    }
                }

                Status.ERROR -> {
                    listener!!.hideLoading()
                    if (isOpenKeyboard()){
                        hideKeyboard()
                    }
                    Snackbar.make(
                        myProductsView,
                        "Ha ocurrido un error al obtener los productos",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })
    }

    private fun setRecyclerView() {
        val linearLayoutManagerStores = LinearLayoutManager(context)
        rvProducts.layoutManager = linearLayoutManagerStores
        rvProducts.adapter = adapter
        adapter.setProductsList(productList)
    }

    override fun onClickProducts(position: Int) {
        val productView = productList[position]
        getDialogAddProduct(
            productView.storeProductId,
            productView.name,
            productView.unitMeasure,
            productView.price
        )
    }

    private fun getDialogAddProduct(
        productId: Int,
        productName: String,
        productUnitMeasure: String,
        productPrice:Double
    ) {
        var hasStock = true

        val dialog = Dialog(context!!)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_dialog_update_product)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val boxPrice = dialog.findViewById(R.id.boxPrice) as TextInputLayout
        val etPrice = dialog.findViewById(R.id.etPrice) as TextInputEditText
        val btnAddProduct = dialog.findViewById(R.id.btnAddProduct) as Button
        val btnNoStock = dialog.findViewById(R.id.btnNoStock) as TextView
        val btnStock = dialog.findViewById(R.id.btnStock) as TextView
        val tvNameProduct = dialog.findViewById(R.id.tvNameProduct) as TextView

        tvNameProduct.text = String.format("%s x %s", productName, productUnitMeasure)
        etPrice.setText(productPrice.toString())
        btnNoStock.setOnClickListener {
            btnNoStock.setTextColor(
                ContextCompat.getColor(
                    context!!,
                    R.color.color_brand_primary_default
                )
            )
            btnStock.setTextColor(ContextCompat.getColor(context!!, R.color.color_text_grey_100))

            hasStock = false
        }

        btnStock.setOnClickListener {
            btnStock.setTextColor(
                ContextCompat.getColor(
                    context!!,
                    R.color.color_brand_primary_default
                )
            )
            btnNoStock.setTextColor(ContextCompat.getColor(context!!, R.color.color_text_grey_100))
            hasStock = true
        }

        btnAddProduct.setOnClickListener {
            if (etPrice.text!!.isNotEmpty()) {
                val price = etPrice.text.toString()
                myProductsViewModel.updateMyProduct(productId, price.toDouble(), getStockForUpdate(hasStock))
                dialog.dismiss()
            } else {
                boxPrice.isErrorEnabled = true
                boxPrice.error = "No ha agregado un precio"
            }
        }

        dialog.show()
    }

    private fun getStockForUpdate(hasStock:Boolean):String{
        return if(hasStock) "S"
        else "N"
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun observerUpdateProducts() {
        myProductsViewModel.updateProduct.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if(it.data){
                        Snackbar.make(
                            myProductsView,
                            it.message!!,
                            Snackbar.LENGTH_SHORT
                        ).show()
                        myProductsViewModel.getMyProducts("")
                    }else{
                        it.message?.let { it1 ->
                            Snackbar.make(
                                myProductsView,
                                it1,
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                Status.ERROR -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        myProductsView,
                        "Ha ocurrido un error al obtener los productos",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })
    }

    private fun onCleanSearch() {
        myProductsViewModel.getMyProducts("")
    }


    interface OnFragmentInteractionListener {
        fun replaceProductsTemplateFragment()
        fun hideLoading()
        fun showLoading()
    }

    fun hideKeyboard() {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0);
    }


    fun isOpenKeyboard(): Boolean {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.isAcceptingText
    }

}
