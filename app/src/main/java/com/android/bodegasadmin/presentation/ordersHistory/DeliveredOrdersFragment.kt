package com.android.bodegasadmin.presentation.ordersHistory

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.presentation.customerOrders.adapter.EstablishmentOrderAdapter
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.adapter.EstablishmentOrderViewHolder
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_delivered_orders.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class DeliveredOrdersFragment : Fragment() , EstablishmentOrderViewHolder.ViewHolderListener {

    private var listener: OnFragmentInteractionListener? = null
    private val establishmentOrdersViewModel: DeliveredOrdersViewModel by sharedViewModel()
    private var establishmentOrdersyViewList: MutableList<EstablishmentOrderView> = mutableListOf()
    private var adapter = EstablishmentOrderAdapter(this)

    companion object {
        const val TAG = "DeliveredOrdersFragment"

        @JvmStatic
        fun newInstance() =
            DeliveredOrdersFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

   /* override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser){
            establishmentOrdersViewModel.getOrdersListByEstablishmentAndState(
                PreferencesManager.getInstance().getEstablishmentSelected().storeId, "T")

        }
    }*/

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_delivered_orders, container, false)
    }

    private fun getInformados() {
        establishmentOrdersViewModel.establishmentCustomerOrders.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    establishmentOrdersyViewList = it.data.toMutableList()
                    if(establishmentOrdersyViewList.isEmpty()){
                        rvListaEntregados.visibility=View.GONE;
                        containerEmptyState.visibility=View.VISIBLE
                    }
                    else{
                        rvListaEntregados.visibility=View.VISIBLE;
                        containerEmptyState.visibility=View.GONE
                        setRecyclerView()
                    }

                }
                Status.ERROR -> {
                    Log.e("Debug", "Status.ERROR")
                    listener?.hideLoading()
                    Snackbar.make(
                        listaEntregadosView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    Log.e("Debug", " Status.LOADING")
                    listener?.showLoading()
                }
            }
        })
        establishmentOrdersViewModel.getOrdersListByEstablishmentAndState(
            PreferencesManager.getInstance().getEstablishmentSelected().storeId, "T") //PreferencesManager.getInstance().getUser().customerId
    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvListaEntregados.layoutManager = linearLayoutManager
        rvListaEntregados.adapter = adapter
        adapter.setStoreList(establishmentOrdersyViewList)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        getInformados()
    }

    override fun onClick(position: Int) {
        val establishmentOrderView = establishmentOrdersyViewList[position]
        listener?.replaceByDeliveredOrdersDetailsFragment(establishmentOrderView, true)
    }

    interface OnFragmentInteractionListener {
        fun replaceByDeliveredOrdersDetailsFragment(establishmentOrderView: EstablishmentOrderView, isFromOrders: Boolean)
        fun showLoading()
        fun hideLoading()
    }
}