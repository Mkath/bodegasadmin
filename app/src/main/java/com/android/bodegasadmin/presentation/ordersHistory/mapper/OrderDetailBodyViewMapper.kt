package com.android.bodegasadmin.presentation.ordersHistory.mapper

import com.android.bodegasadmin.domain.establishmentOrders.OrderDetailBody
import com.android.bodegasadmin.presentation.ordersHistory.model.OrderDetailBodyView


class OrderDetailBodyViewMapper : SaveViewMapper<OrderDetailBodyView, OrderDetailBody> {
    override fun mapToUseCase(type: OrderDetailBodyView): OrderDetailBody {
        return OrderDetailBody(
            type.orderDetailId,
            type.state
        )
    }

    override fun mapToView(type: OrderDetailBody): OrderDetailBodyView {
        return OrderDetailBodyView(
            type.orderDetailId,
            type.state
        )
    }

}
