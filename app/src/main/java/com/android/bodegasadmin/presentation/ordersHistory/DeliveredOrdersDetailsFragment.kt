package com.android.bodegasadmin.presentation.ordersHistory

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.adapter.OrderDetailAdapter
import com.android.bodegasadmin.presentation.ordersHistory.adapter.OrderDetailViewHolder
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.presentation.utils.DateTimeHelper
import kotlinx.android.synthetic.main.fragment_approved_orders_details.*
import kotlinx.android.synthetic.main.fragment_approved_orders_details.boxDelivery
import kotlinx.android.synthetic.main.fragment_approved_orders_details.boxStatus
import kotlinx.android.synthetic.main.fragment_approved_orders_details.imSend
import kotlinx.android.synthetic.main.fragment_approved_orders_details.rvCustomerOrdersDetail
import kotlinx.android.synthetic.main.fragment_approved_orders_details.titleDeliveryCharge
import kotlinx.android.synthetic.main.fragment_approved_orders_details.titleSendDate
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvClientName
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvDeliveryCharge
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvDirection
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvFechaUpdate
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvOrderDate
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvPM
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvPhone
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvSendDate
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvTitlePayment
import kotlinx.android.synthetic.main.fragment_approved_orders_details.tvTotalOrder
import kotlinx.android.synthetic.main.fragment_delivered_orders_details.*
import kotlinx.android.synthetic.main.fragment_informed_orders_details.*
import java.math.BigDecimal

private val ORDER = "orderDetails"

class DeliveredOrdersDetailsFragment : Fragment(), OrderDetailViewHolder.ViewHolderListener {

    private lateinit var order: EstablishmentOrderView
    private var adapter = OrderDetailAdapter("T",this)
    private var listener: OnFragmentInteractionListener? = null
    private val REQUEST_CODE_STORAGE = 200

    companion object {
        const val TAG = "DeliveredOrdersDetailsFragment"

        @JvmStatic
        fun newInstance(establishmentOrderView: EstablishmentOrderView) =
            DeliveredOrdersDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ORDER, establishmentOrderView)
                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            order = it.getParcelable(ORDER)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_delivered_orders_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
    }

    private fun setButtonPhoneIconToCall() {
        imPhoneDelivered.setOnClickListener {
            if (validateCallPermissionsStorage()) {
                makeCallWhenClickOn()
            } else {
                requestCallPermissions()
            }
        }
    }

    private fun validateCallPermissionsStorage(): Boolean {
        return ActivityCompat.checkSelfPermission(
            activity!!.applicationContext,
            android.Manifest.permission.CALL_PHONE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestCallPermissions() {
        val contextProvider =
            ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                android.Manifest.permission.CALL_PHONE
            )

        if (contextProvider) {
            Toast.makeText(
                activity!!.applicationContext,
                "Los permisos son requeridos para llamar al cliente",
                Toast.LENGTH_SHORT
            ).show()
        }
        permissionRequest()
    }

    private fun permissionRequest() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(android.Manifest.permission.CALL_PHONE),
            REQUEST_CODE_STORAGE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    makeCallWhenClickOn()
                } else {
                    Toast.makeText(
                        activity!!.applicationContext,
                        "No aceptó los permisos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

    }

    private fun makeCallWhenClickOn() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:"+order.customerPhoneNumber))
        startActivity(intent)
    }

    private fun setView() {

        tvDirection.text = order.customerAddress + "\n" + order.customerUrbanization
        tvClientName.text = order.customerCompleteName
        tvPhone.text = order.customerPhoneNumber
        tvOrderDate.text = DateTimeHelper.parseDateOrder(order.creationDate)
        tvFechaUpdate.text = DateTimeHelper.parseDateOrder(order.updateDate)
        setButtonPhoneIconToCall()
        if(order.isDelivery){
            tvSendDate.visibility = View.VISIBLE
            titleSendDate.visibility = View.VISIBLE
            tvSendDate.text = order.shippingDateFrom
            imSend.visibility = View.VISIBLE
        }else{
            tvSendDate.visibility = View.GONE
            titleSendDate.visibility = View.GONE
            imSend.visibility = View.GONE
        }
        tvTotalOrder.text = String.format("%s %s", "s/", order.total)
        tvPM.text = order.paymentMethodDescription
        var aux:BigDecimal = order.customerAmount.toBigDecimal() - order.total.toBigDecimal()
        var aux2:BigDecimal = order.customerAmount.toBigDecimal() - order.total.toBigDecimal()

        aux = String.format("%.2f", aux).toBigDecimal()
        var inputTotalVuelto1 =String.format("%s %s", "s/ ", aux.toString())

        if(aux2.compareTo(BigDecimal.ZERO) <0) {
            inputTotalVuelto1 = inputTotalVuelto1 + " El vuelto negativo se origina por el 'Cargo por envío'"
        }
        /* var inputTotalVuelto1="El vuelvo negativo se origina por el 'Cargo por envío'"
         if(aux.compareTo(BigDecimal.ZERO)>1) {
             aux = String.format("%.2f", aux).toBigDecimal()
             inputTotalVuelto1 =String.format("%s %s", "s/ ", aux.toString())
         }*/

        boxDelivery.text = order.deliveryType
        boxDelivery.isEnabled = false
        boxStatus.text = String.format("%s %s", "Pedido", order.stringStatus)

        if (order.isDelivery){
            titleDeliveryCharge.visibility = View.VISIBLE
            tvDeliveryCharge.visibility = View.VISIBLE
            if (order.deliveryCharge != "0.00"){
                tvDeliveryCharge.text = String.format("%s %s", "s/", order.deliveryCharge )
            }
        }else{
            titleDeliveryCharge.visibility = View.GONE
            tvDeliveryCharge.visibility = View.GONE
        }


        if (order.paymentMethodDescription == "Efectivo"){
            titleTotalVuelo.visibility = View.VISIBLE
            titleTotalVuelo.text = String.format("%s %s %s", "Paga con: "," s/", order.customerAmount)
            var paga1 = String.format("%s %s %s", "Paga con:"," s/", order.customerAmount)
            titleTotalVuelo.text = paga1 + " Vuelto: " + inputTotalVuelto1
        }else{
            titleTotalVuelo.visibility = View.GONE
            titleTotalVuelo.text = ""
        }
        setRecyclerView()
    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvCustomerOrdersDetail.layoutManager = linearLayoutManager
        rvCustomerOrdersDetail.adapter = adapter
        adapter.setStoreList(order.orderDetails)
    }

    override fun onClick(position: Int) {

    }

    interface OnFragmentInteractionListener {
        fun onBackPressed()
        fun showLoading()
        fun hideLoading()
    }
}