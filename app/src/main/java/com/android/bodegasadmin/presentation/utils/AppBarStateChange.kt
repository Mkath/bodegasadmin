package com.android.bodegasadmin.presentation.utils

import com.google.android.material.appbar.AppBarLayout


open class AppBarStateChange(private val onOffsetChanged: AppBarStateChangeListener) :
    AppBarLayout.OnOffsetChangedListener {

    private var currentState = State.IDLE


    enum class State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, i: Int) {
        when {
            i == 0 -> {
                if (currentState != State.EXPANDED) {
                    onOffsetChanged.onStateChanged(appBarLayout, State.EXPANDED)
                }
                currentState = State.EXPANDED
            }
            Math.abs(i) >= appBarLayout.totalScrollRange -> {
                if (currentState != State.COLLAPSED) {
                    onOffsetChanged.onStateChanged(appBarLayout, State.COLLAPSED)
                }
                currentState = State.COLLAPSED
            }
            else -> {
                if (currentState != State.IDLE) {
                    onOffsetChanged.onStateChanged(appBarLayout, State.IDLE)
                }
                currentState = State.IDLE
            }
        }
    }

    interface AppBarStateChangeListener {
        fun onStateChanged(appBarLayout: AppBarLayout, state: State)
    }
}