package com.android.bodegasadmin.presentation.splash

import android.os.Bundle
import android.os.Handler
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.presentation.BaseActivity
import com.android.bodegasadmin.presentation.login.LoginActivity
import com.android.bodegasadmin.presentation.main.MainActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        showView()
    }

    private fun showView() {
        val handler = Handler()
        handler.postDelayed({
            if (PreferencesManager.getInstance().isLogin()) {
                loginSuccessfully()
            } else {
                nextActivity(null, LoginActivity::class.java, true)
            }

        }, 1000)
    }


    private fun loginSuccessfully() {
        nextActivity(null, MainActivity::class.java, true)
    }

}
