package com.android.bodegasadmin.presentation.ordersHistory.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EstablishmentOrderView (
    val creationDate:String,
    val orderId: String,
    val deliveryType: String,
    val isDelivery:Boolean,
    val shippingDateFrom: String,
    val status: String,
    val stringStatus: String,
    val establishmentEmail: String,
    val establishmentName: String,
    val total: String,
    val customerPhoneNumber: String,
    val customerCompleteName: String,
    val customerAddress:String,
    val customerLatitude: Double,
    val customerLongitude: Double,
    val orderDetails:List<OrdersItemView>,
    val updateDate: String,
    val paymentMethodCustomerMessage: String,
    val paymentMethodDescription: String,
    val paymentMethodEstablishmentMessage: String,
    val paymentMethodPaymentMethodId: Int,
    val paymentMethodRequestAmount: Boolean,
    val customerAmount: String,
    val deliveryCharge: String,
    val customerUrbanization: String
) : Parcelable