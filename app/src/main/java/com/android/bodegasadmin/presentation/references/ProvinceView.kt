package com.android.bodegasadmin.presentation.references

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProvinceView (
    val id: String,
    val name: String,
    val department_id: String
) : Parcelable