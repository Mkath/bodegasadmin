package com.android.bodegasadmin.presentation.establishment.mapper

import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.domain.establishment.Establishment
import com.android.bodegasadmin.domain.paymentMethod.PaymentMethod
import com.android.bodegasadmin.domain.schedule.Schedule
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentView
import com.android.bodegasadmin.presentation.establishment.model.PaymentMethodView
import com.android.bodegasadmin.presentation.establishment.model.ScheduleView

open class EstablishmentViewMapper (
    private val scheduleViewMapper: ScheduleViewMapper,
    private val paymentMethodViewMapper: PaymentMethodViewMapper
) :
    Mapper<EstablishmentView, Establishment> {

    override fun mapToView(type: Establishment): EstablishmentView {
        return EstablishmentView(
            type.establishmentId,
            type.businessName,
            type.address,
            type.email,
            type.latitude,
            type.longitude,
            getListSchedules(type.shippingSchedule),
            getListSchedules(type.operationSchedule),
            type.dni,
            type.pathImage,
            getHasDelivery(type.delivery),
            type.businessName,
            type.departament,
            type.province,
            type.district,
            type.dni,
            type.urbanization,
            type.name,
            type.lastNamePaternal,
            type.lastNameMaternal,
            type.dni,
            type.phoneNumber,
            type.ruc,
            type.establishmentType,
            type.deliveryCharge,
            getListPaymentMethods(type.paymentMethod)
        )
    }

    private fun getHasDelivery(hasDelivery:String):Boolean{
        return hasDelivery == "S"
    }

    private fun getListSchedules(list: List<Schedule>): List<ScheduleView>{
        val listSchedule = mutableListOf<ScheduleView>()
        list.forEach {
            listSchedule.add(scheduleViewMapper.mapToView(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethods(list: List<PaymentMethod>): List<PaymentMethodView>{
        val listPaymentMethods = mutableListOf<PaymentMethodView>()
        list.forEach {
            listPaymentMethods.add(paymentMethodViewMapper.mapToView(it))
        }
        return listPaymentMethods
    }

}