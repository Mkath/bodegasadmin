package com.android.bodegasadmin.presentation.establishment.mapper

interface SaveViewMapper<V, D> {

    fun mapToUseCase(type: V): D

    fun mapToView(type: D): V
}