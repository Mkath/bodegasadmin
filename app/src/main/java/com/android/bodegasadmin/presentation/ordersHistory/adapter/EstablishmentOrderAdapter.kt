package com.android.bodegas.presentation.customerOrders.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegasadmin.R
import com.android.bodegasadmin.presentation.ordersHistory.adapter.EstablishmentOrderViewHolder
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.presentation.utils.DateTimeHelper

class EstablishmentOrderAdapter  (private val viewHolderListener: EstablishmentOrderViewHolder.ViewHolderListener):
RecyclerView.Adapter<EstablishmentOrderViewHolder>() {

    private val customerOrdersList = mutableListOf<EstablishmentOrderView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EstablishmentOrderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_customer_order, parent, false)
        context = parent.context
        return EstablishmentOrderViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return customerOrdersList.size
    }

    override fun onBindViewHolder(holder: EstablishmentOrderViewHolder, position: Int) {
        val establishmentOrderView = customerOrdersList[position]
        holder.tvUserName.text = establishmentOrderView.customerCompleteName
        holder.tvTotal.text = String.format("%s %s", "s/ ", establishmentOrderView.total)
        holder.tvDate.text = DateTimeHelper.parseDateOrder(establishmentOrderView.creationDate)
        if(establishmentOrderView.isDelivery){
            holder.tvSendDate.visibility = View.VISIBLE
            holder.tvTitleSendDate.visibility = View.VISIBLE
            holder.tvSendDate.text =  establishmentOrderView.shippingDateFrom
        }else{
            holder.tvSendDate.visibility = View.GONE
            holder.tvTitleSendDate.visibility = View.GONE
        }

        holder.tvDeliveryType.text = establishmentOrderView.deliveryType
    }

    fun setStoreList(poolList: List<EstablishmentOrderView>) {
        this.customerOrdersList.clear()
        this.customerOrdersList.addAll(poolList)
    }
}