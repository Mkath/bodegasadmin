package com.android.bodegasadmin.presentation.allSchedules

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.domain.allschedule.GetAllScheduleByType
import com.android.bodegasadmin.domain.establishment.register.RegisterNewEstablishment
import com.android.bodegasadmin.domain.references.department.GetDepartments
import com.android.bodegasadmin.domain.references.district.GetDistricts
import com.android.bodegasadmin.domain.references.province.GetProvinces
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.allSchedules.mapper.AllScheduleViewMapper
import com.android.bodegasadmin.presentation.allSchedules.model.AllScheduleView
import com.android.bodegasadmin.presentation.establishment.mapper.EstablishmentViewMapper
import com.android.bodegasadmin.presentation.establishment.mapper.RegisterEstablishmentBodyViewMapper
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentView
import com.android.bodegasadmin.presentation.establishment.model.RegisterEstablishmentBodyView
import com.android.bodegasadmin.presentation.references.DepartmentView
import com.android.bodegasadmin.presentation.references.DistrictView
import com.android.bodegasadmin.presentation.references.ProvinceView
import com.android.bodegasadmin.presentation.references.mapper.DepartmentViewMapper
import com.android.bodegasadmin.presentation.references.mapper.DistrictViewMapper
import com.android.bodegasadmin.presentation.references.mapper.ProvinceViewMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class GetAllSchedulesViewModel constructor(
    private val getAllScheduleByType: GetAllScheduleByType,
    private val getAllScheduleViewMapper: AllScheduleViewMapper
) :
    ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _operationSchedule = MutableLiveData<Resource<List<AllScheduleView>>>()
    val operationSchedule: LiveData<Resource<List<AllScheduleView>>> = _operationSchedule

    private val _deliverySchedule = MutableLiveData<Resource<List<AllScheduleView>>>()
    val deliverySchedule: LiveData<Resource<List<AllScheduleView>>> = _deliverySchedule

    fun getOperationSchedule(type: String) {
        _operationSchedule.value = Resource(Status.LOADING, mutableListOf(), "")
        viewModelScope.launch {
            val allScheduleResult = getAllScheduleByType.getAllScheduleByType(type)

            if (allScheduleResult.status == Status.SUCCESS) {
                val allScheduleResultData = allScheduleResult.data

                if (allScheduleResultData.isNotEmpty()) {
                    val allScheduleViewList =
                        allScheduleResultData.map { getAllScheduleViewMapper.mapToView(it) }
                    _operationSchedule.value = Resource(Status.SUCCESS, allScheduleViewList, "")
                } else {
                    _operationSchedule.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }

            } else {
                _operationSchedule.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun getDeliverySchedule(type: String) {
        _deliverySchedule.value = Resource(Status.LOADING, mutableListOf(), "")
        viewModelScope.launch {
            val allScheduleResult = getAllScheduleByType.getAllScheduleByType(type)

            if (allScheduleResult.status == Status.SUCCESS) {
                val allScheduleResultData = allScheduleResult.data

                if (allScheduleResultData.isNotEmpty()) {
                    val allScheduleViewList =
                        allScheduleResultData.map { getAllScheduleViewMapper.mapToView(it) }
                    _deliverySchedule.value = Resource(Status.SUCCESS, allScheduleViewList, "")
                } else {
                    _deliverySchedule.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }

            } else {
                _deliverySchedule.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }
}