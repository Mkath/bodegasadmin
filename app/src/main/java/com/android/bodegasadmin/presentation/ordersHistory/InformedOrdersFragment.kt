package com.android.bodegasadmin.presentation.ordersHistory

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.presentation.customerOrders.adapter.EstablishmentOrderAdapter
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.adapter.EstablishmentOrderViewHolder
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_informed_orders.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class InformedOrdersFragment : Fragment() , EstablishmentOrderViewHolder.ViewHolderListener {

    private var listener: OnFragmentInteractionListener? = null
    private val informedOrdersViewModel: InformedOrdersViewModel by sharedViewModel()
    private var establishmentOrdersViewList: MutableList<EstablishmentOrderView> = mutableListOf()
    private var adapter = EstablishmentOrderAdapter(this)

    companion object {
        const val TAG = "InformedOrdersFragment"

        @JvmStatic
        fun newInstance() =
            InformedOrdersFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_informed_orders, container, false)
    }

    private fun getInformedOrders() {
        informedOrdersViewModel.establishmentCustomerOrders.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    establishmentOrdersViewList = it.data.toMutableList()
                    if(establishmentOrdersViewList.isEmpty()){
                        rvListaInformados.visibility=View.GONE;
                        containerEmptyState.visibility=View.VISIBLE
                    }
                    else{
                        rvListaInformados.visibility=View.VISIBLE;
                        containerEmptyState.visibility=View.GONE
                        setRecyclerView()
                    }
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        listaInformadosView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        informedOrdersViewModel.getOrdersListByEstablishmentAndState(PreferencesManager.getInstance().getEstablishmentSelected().storeId, "I") //PreferencesManager.getInstance().getUser().customerId
    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvListaInformados.layoutManager = linearLayoutManager
        rvListaInformados.adapter = adapter
        adapter.setStoreList(establishmentOrdersViewList)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        getInformedOrders()
    }

    override fun onClick(position: Int) {
        val establishmentOrderView = establishmentOrdersViewList[position]
        listener?.replaceByInformedOrdersDetailsFragment(establishmentOrderView, true)
    }

    interface OnFragmentInteractionListener {
        fun replaceByInformedOrdersDetailsFragment(establishmentOrderView: EstablishmentOrderView, isFromOrders:Boolean)
        fun showLoading()
        fun hideLoading()
    }


}