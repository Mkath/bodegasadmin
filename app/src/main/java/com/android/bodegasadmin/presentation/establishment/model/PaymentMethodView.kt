package com.android.bodegasadmin.presentation.establishment.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentMethodView(
    val paymentMethodId: Int,
    val requestedAmount: Boolean,
    val description: String,
    val customerMessage: String
) : Parcelable