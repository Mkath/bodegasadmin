package com.android.bodegasadmin.presentation.ordersHistory

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.android.bodegas.presentation.customerOrders.adapter.EstablishmentOrderAdapter
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.R
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.adapter.EstablishmentOrderViewHolder
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegasadmin.data.PreferencesManager
import kotlinx.android.synthetic.main.fragment_approved_orders.*
import kotlinx.android.synthetic.main.fragment_approved_orders.containerEmptyState
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ApprovedOrdersFragment : Fragment() , EstablishmentOrderViewHolder.ViewHolderListener {

    private var listener: OnFragmentInteractionListener? = null
    private val customerOrdersViewModel: ApprovedOrdersViewModel by sharedViewModel()

    private var customerOrdersyViewList: MutableList<EstablishmentOrderView> = mutableListOf()
    private var adapter = EstablishmentOrderAdapter(this)

    private val ESTADO_ATENDIDO:String ="A"
    private val ESTADO_ENVIADO:String ="E"

    companion object {
        const val TAG = "ApprovedOrdersFragment"

        @JvmStatic
        fun newInstance() =
            ApprovedOrdersFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_approved_orders, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerGetOrders()
        botonEnviado.setOnClickListener{
            botonEnviado.background = resources.getDrawable( R.drawable.button_rounded_purple_background)
            botonAtendido.background = resources.getDrawable( R.drawable.button_rounded_grey_background)
            getAprobados(ESTADO_ENVIADO)
        }

        botonAtendido.setOnClickListener{
            botonEnviado.background = resources.getDrawable( R.drawable.button_rounded_grey_background)
            botonAtendido.background =resources.getDrawable( R.drawable.button_rounded_purple_background)
            getAprobados(ESTADO_ATENDIDO)
        }


    }

    private fun observerGetOrders(){
        customerOrdersViewModel.establishmentCustomerOrders.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    customerOrdersyViewList = it.data.toMutableList()
                    if(customerOrdersyViewList.isEmpty()){
                        rvListaAprobados.visibility=View.GONE;
                       // botones.visibility=View.VISIBLE;
                        containerEmptyState.visibility=View.VISIBLE
                    }
                    else{
                        rvListaAprobados.visibility=View.VISIBLE;
                     //   botones.visibility=View.VISIBLE;
                        containerEmptyState.visibility=View.GONE
                        setRecyclerView()
                    }
                }
                Status.ERROR -> {
                    Log.e("Debug", "Status.ERROR")
                    listener?.hideLoading()
                    Snackbar.make(
                        listaAprobadosView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    Log.e("Debug", " Status.LOADING")
                    listener?.showLoading()
                }
            }
        })
    }

    private fun getAprobados(estado: String) {
        customerOrdersViewModel.getOrdersListByEstablishmentAndState(
        PreferencesManager.getInstance().getEstablishmentSelected().storeId, estado) //PreferencesManager.getInstance().getUser().customerId

    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvListaAprobados.layoutManager = linearLayoutManager
        rvListaAprobados.adapter = adapter
        adapter.setStoreList(customerOrdersyViewList)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        getAprobados(ESTADO_ATENDIDO)
    }

    override fun onClick(position: Int) {
        val establishmentOrderView = customerOrdersyViewList[position]
        listener?.replaceByApprovedOrdersDetailsFragment(establishmentOrderView, true)
    }

    interface OnFragmentInteractionListener {
        fun replaceByApprovedOrdersDetailsFragment(establishmentOrderView: EstablishmentOrderView, isFromOrders: Boolean)
        fun showLoading()
        fun hideLoading()
    }
}