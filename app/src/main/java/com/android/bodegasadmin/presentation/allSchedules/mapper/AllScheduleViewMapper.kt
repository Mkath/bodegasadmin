package com.android.bodegasadmin.presentation.allSchedules.mapper

import com.android.bodegasadmin.domain.allschedule.AllSchedule
import com.android.bodegasadmin.domain.schedule.Schedule
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.allSchedules.model.AllScheduleView
import com.android.bodegasadmin.presentation.establishment.model.ScheduleView


class AllScheduleViewMapper : Mapper<AllScheduleView, AllSchedule> {

    override fun mapToView(type: AllSchedule): AllScheduleView {
        return AllScheduleView(
            type.scheduleId,
            type.range,
            type.type,
            type.startTime,
            type.endTime
        )
    }

}