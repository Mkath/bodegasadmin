package com.android.bodegasadmin.presentation.products_template.adapter


import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import com.android.bodegasadmin.R
import com.android.bodegasadmin.presentation.products_template.model.ProductsView


class ProductsAdapter(
    private val viewHolderListener: ProductsViewHolder.ViewHolderListener
) : RecyclerView.Adapter<ProductsViewHolder>() {

    private val productsList = mutableListOf<ProductsView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_products, parent, false)
        context = parent.context
        return ProductsViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        val product = productsList[position]
        holder.bind(product, context)

        /*  if (store.hasDelivery) {
              holder.storeDelivery.visibility = View.VISIBLE
          } else {
              holder.storeDelivery.visibility = View.GONE
          }*/
    }


    fun setProductsList(poolList: List<ProductsView>) {
        this.productsList.clear()
        this.productsList.addAll(poolList)
        notifyDataSetChanged()
    }


}