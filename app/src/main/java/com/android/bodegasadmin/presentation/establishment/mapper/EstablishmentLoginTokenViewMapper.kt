package com.android.bodegasadmin.presentation.establishment.mapper

import com.android.bodegasadmin.domain.establishment.EstablishmentLoginToken
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.establishment.model.EstablishmentLoginTokenView

open class EstablishmentLoginTokenViewMapper(): Mapper<EstablishmentLoginTokenView, EstablishmentLoginToken> {
    override fun mapToView(type: EstablishmentLoginToken): EstablishmentLoginTokenView {
        return EstablishmentLoginTokenView(
            type.idEntity,
            type.token
        )
    }
}