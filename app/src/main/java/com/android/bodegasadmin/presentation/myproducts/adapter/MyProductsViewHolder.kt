package com.android.bodegasadmin.presentation.myproducts.adapter

import android.content.Context
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.presentation.myproducts.model.MyProductsView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.row_my_products.view.*

class MyProductsViewHolder(itemView: View, viewHolderListener: ViewHolderListener) :
    RecyclerView.ViewHolder(itemView) {
    val productName: TextView = itemView.tvProductName
    val productDescription: TextView = itemView.tvDescriptionProduct
    val productPrice: TextView = itemView.tvPriceProduct
    val imageProduct: ImageView = itemView.ivImageProduct
    val tvStock: TextView = itemView.tvStock
    val holder: ViewHolderListener = viewHolderListener

    fun bind(product: MyProductsView, context: Context) {
        productName.text = product.name
        productDescription.text = product.description
        productPrice.text = String.format("%s %.2f", "s/", product.price)
        if(product.stock.isNotEmpty()){
            tvStock.visibility = View.VISIBLE
            tvStock.text = product.stock
        }else{
            tvStock.visibility = View.GONE
        }

        if (product.imageProduct!!.isNotEmpty()) {

            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            Glide.with(context)  // this
                .load(product.imageProduct)
                .placeholder(circularProgressDrawable)
                .error(context.getDrawable(R.drawable.ic_image_black_24dp))
                .into(imageProduct)
        }


        itemView.setOnClickListener {
            holder.onClickProducts(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClickProducts(
            position: Int
        )
    }
}

