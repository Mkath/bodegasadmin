package com.android.bodegasadmin.presentation.allSchedules.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllScheduleView(
    val scheduleId: Int,
    val range: String,
    val type:String,
    val startTime: String ? = "",
    val endTime: String ? = ""
) : Parcelable