package com.android.bodegasadmin.presentation.ordersHistory.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrdersItemView (
    val orderDetailOrderDetailId: String,
    val orderDetailUnitMeasure: String,
    val orderDetailQuantity: String,
    val orderDetailPrice: String,
    val orderDetailStatus: String,
    val orderDetailObservation: String,
    val orderDetailSubTotal: String,
    val storeProductStoreProductId: String,
    val storeProductPrice: String,
    val storeProductStatus: String,
    val productTemplateProductTemplateId: String,
    val productTemplateCode: String,
    val productTemplateName: String,
    val productTemplateDescription: String,
    val productTemplateUnitMeasure: String,
    val productTemplateStatus: String,
    val productTemplatePathImage: String,
    var isAttended: Boolean
) : Parcelable
