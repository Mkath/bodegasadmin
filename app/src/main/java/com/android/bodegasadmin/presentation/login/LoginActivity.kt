package com.android.bodegasadmin.presentation.login

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.BaseActivity
import com.android.bodegasadmin.presentation.registerEstablishment.RegisterBasicDataActivity
import com.android.bodegasadmin.presentation.main.MainActivity
import com.android.bodegasadmin.presentation.utils.animation.LoadingDots
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginActivity : BaseActivity() {

    private val loginViewModel: LoginViewModel by viewModel()
    private lateinit var validationUtil: InputValidationUtil
    private var loadingDots: LoadingDots? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        observeViewModel()
        observeViewModelGetData()
        setButtonClickListener()
        recoveryPassword()
        goToRegisterActivity()
        validationUtil = InputValidationUtil(
            etUser,
            textInputLayoutUser,
            etPass,
            textInputLayoutPassword,
            applicationContext,
            btnLogin,
            imageView
        )

        layoutProgressBar.visibility = View.GONE
    }

    private fun observeViewModel() {
        loginViewModel.loginCodeToken.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                   PreferencesManager.getInstance().setIsLogin(true)
                    showErrorMessage(
                        it.message.let { "Login correcto"})
                    //loginSuccessfully()
                    loginViewModel.getDataEstablishment(
                        PreferencesManager.getInstance().getEstablishmentLoginTokenSelected().idEntity.toInt()
                    )
                }
                Status.ERROR -> {
                    layoutProgressBar.visibility = View.GONE
                    if (it.message == "500") {
                        showErrorMessage(
                            it.message.let { getString(R.string.login_not_connection_message)})
                    } else {
                        it.message?.let { it1 ->
                            showErrorMessage(
                                it1
                            )
                        }
                    }
                }
                Status.LOADING -> {
                    layoutProgressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun observeViewModelGetData() {
        loginViewModel.loginCode.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    PreferencesManager.getInstance().setIsLogin(true)
                    showErrorMessage(
                        it.message.let { "Obtención de datos correctamente realizado"})
                    loginSuccessfully()
                }
                Status.ERROR -> {
                    layoutProgressBar.visibility = View.GONE
                    if (it.message == "500") {
                        showErrorMessage(
                            it.message.let { getString(R.string.login_not_connection_message)})
                    } else {
                        it.message?.let { it1 ->
                            showErrorMessage(
                                it1
                            )
                        }
                    }
                }
                Status.LOADING -> {
                    layoutProgressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun animateLoadingText() {
        if (loadingDots == null) {
            loadingDots =
                LoadingDots(dots)
            loadingDots?.animateLoadingText()
        }
    }

    private fun setButtonClickListener() {
        btnLogin.setOnClickListener {
            layoutProgressBar.visibility = View.VISIBLE
            if (isOpenKeyboard()) {
                hideKeyboard()
                imageView.visibility = View.VISIBLE
            }
            if (validationUtil.getValidate()) {
                layoutProgressBar.visibility = View.VISIBLE
                animateLoadingText()
                loginViewModel.loginEstablishmentToken(etUser.text.toString(), etPass.text.toString())

            }else{
                layoutProgressBar.visibility = View.GONE
            }
        }

    }

    private fun loginSuccessfully() {
        layoutProgressBar.visibility = View.GONE
        val bundle = Bundle()
        nextActivity(bundle, MainActivity::class.java, true)
    }

    private fun showErrorMessage(message: String) {
        Snackbar.make(
            mainView,
            message,
            Snackbar.LENGTH_LONG
        ).show()
    }

    private fun goToRegisterActivity(){
        btnRegistrarse.setOnClickListener{
            if(isOpenKeyboard()){
                hideKeyboard()
            }
            nextActivity(null, RegisterBasicDataActivity::class.java, true)
        }
    }

    private fun recoveryPassword(){
        btnGetPassword.setOnClickListener{
                if(isOpenKeyboard()){
                    hideKeyboard()
                }
                nextActivity(null, RecoveryPasswordActivity::class.java, true)
        }
    }


}
