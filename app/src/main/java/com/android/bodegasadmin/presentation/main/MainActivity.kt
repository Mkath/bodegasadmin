package com.android.bodegasadmin.presentation.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.android.bodegasadmin.R
import com.android.bodegasadmin.presentation.myproducts.MyProductsFragment
import com.android.bodegasadmin.presentation.ordersHistory.*
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.presentation.products_template.ProductsFragment
import com.android.bodegasadmin.presentation.profile.ProfileFragment
import com.android.bodegasadmin.presentation.searchOrders.SearchOrdersFragment
import com.android.bodegasadmin.presentation.utils.animation.LoadingAnimationImageView
import com.android.bodegasadmin.presentation.utils.animation.LoadingDots
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_loading.*

const val TITLE_SEARCH = "Buscar pedidos"
const val TITLE_MY_PRODUCTS = "Mis Productos"
const val TITLE_ADD_PRODUCTS = "Agregar Productos"
const val TITLE_ORDERS_HISTORY = "Historial de pedidos"
const val TITLE_PROFILE = "Perfil"
const val TITLE_LIST_DETAIL = "Detalle de pedido"

class MainActivity : AppCompatActivity(),
    SearchOrdersFragment.OnFragmentInteractionListener,
    MyProductsFragment.OnFragmentInteractionListener,
    OrdersHistory.OnFragmentInteractionListener,
    ProfileFragment.OnFragmentInteractionListener,
    ApprovedOrdersFragment.OnFragmentInteractionListener,
    ApprovedOrdersDetailsFragment.OnFragmentInteractionListener,
    CanceledOrdersFragment.OnFragmentInteractionListener,
    CanceledOrdersDetailsFragment.OnFragmentInteractionListener,
    DeliveredOrdersFragment.OnFragmentInteractionListener,
    DeliveredOrdersDetailsFragment.OnFragmentInteractionListener,
    InformedOrdersFragment.OnFragmentInteractionListener,
    InformedOrdersDetailsFragment.OnFragmentInteractionListener,
    ProductsFragment.OnFragmentInteractionListener {

    private var loadingAnimationImageView: LoadingAnimationImageView? = null
    private var loadingDots: LoadingDots? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottom_navigation.selectedItemId = R.id.item_search
        llBack.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onResume() {
        super.onResume()
        when (bottom_navigation.selectedItemId) {
            R.id.item_profile -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutProfile) as ProfileFragment
                fragment.refreshData()
            }
            R.id.item_search -> {
                val fragment = supportFragmentManager.findFragmentById(R.id.frameLayoutSearchOrders) as SearchOrdersFragment
                fragment.refreshList()
            }
        }
    }

    private fun setSearchOrdersFragment() {
        llBack.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_SEARCH
        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutSearchOrders,
                SearchOrdersFragment.newInstance(),
                SearchOrdersFragment.TAG
            )
            .addToBackStack(SearchOrdersFragment.TAG).commitAllowingStateLoss()
    }

    private fun replaceOrdersHistoryFragments() {
        llBack.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_ORDERS_HISTORY

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutOrders,
                OrdersHistory.newInstance(),
                OrdersHistory.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .commit()
    }

    private fun replaceMyProductsFragments() {
        llBack.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_MY_PRODUCTS

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutProducts,
                MyProductsFragment.newInstance(),
                MyProductsFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            ).commit()
    }


    private fun replaceProfileFragment() {
        llBack.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_PROFILE

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutProfile,
                ProfileFragment.newInstance(),
                ProfileFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .commit()
    }


    override fun replaceByApprovedOrdersDetailsFragment(
        establishmentOrderView: EstablishmentOrderView,
        isFromOrders: Boolean
    ) {
        llBack.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_LIST_DETAIL

        if (isFromOrders) {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frameLayoutOrders,
                    ApprovedOrdersDetailsFragment.newInstance(establishmentOrderView),
                    ApprovedOrdersDetailsFragment.TAG
                ).setCustomAnimations(
                    R.anim.slide_enter_from_right,
                    R.anim.slide_exit_to_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                .hide(supportFragmentManager.findFragmentByTag(OrdersHistory.TAG)!!)
                .addToBackStack(ApprovedOrdersDetailsFragment.TAG).commitAllowingStateLoss()

        } else {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frameLayoutSearchOrders,
                    ApprovedOrdersDetailsFragment.newInstance(establishmentOrderView),
                    ApprovedOrdersDetailsFragment.TAG
                ).setCustomAnimations(
                    R.anim.slide_enter_from_right,
                    R.anim.slide_exit_to_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                .hide(supportFragmentManager.findFragmentByTag(SearchOrdersFragment.TAG)!!)
                .addToBackStack(ApprovedOrdersDetailsFragment.TAG).commitAllowingStateLoss()
        }

    }

    override fun replaceByCanceledDetailsOrderFragment(
        establishmentOrderView: EstablishmentOrderView,
        isFromOrders: Boolean
    ) {
        llBack.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_LIST_DETAIL

        if (isFromOrders) {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frameLayoutOrders,
                    CanceledOrdersDetailsFragment.newInstance(establishmentOrderView),
                    CanceledOrdersDetailsFragment.TAG
                ).setCustomAnimations(
                    R.anim.slide_enter_from_right,
                    R.anim.slide_exit_to_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                .hide(supportFragmentManager.findFragmentByTag(OrdersHistory.TAG)!!)
                .addToBackStack(CanceledOrdersDetailsFragment.TAG).commitAllowingStateLoss()
        } else {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frameLayoutSearchOrders,
                    CanceledOrdersDetailsFragment.newInstance(establishmentOrderView),
                    CanceledOrdersDetailsFragment.TAG
                ).setCustomAnimations(
                    R.anim.slide_enter_from_right,
                    R.anim.slide_exit_to_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                .hide(supportFragmentManager.findFragmentByTag(SearchOrdersFragment.TAG)!!)
                .addToBackStack(CanceledOrdersDetailsFragment.TAG).commitAllowingStateLoss()
        }

    }

    override fun replaceByDeliveredOrdersDetailsFragment(
        establishmentOrderView: EstablishmentOrderView,
        isFromOrders: Boolean
    ) {
        llBack.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_LIST_DETAIL

        if (isFromOrders) {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frameLayoutOrders,
                    DeliveredOrdersDetailsFragment.newInstance(establishmentOrderView),
                    DeliveredOrdersDetailsFragment.TAG
                ).setCustomAnimations(
                    R.anim.slide_enter_from_right,
                    R.anim.slide_exit_to_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                .hide(supportFragmentManager.findFragmentByTag(OrdersHistory.TAG)!!)
                .addToBackStack(DeliveredOrdersDetailsFragment.TAG).commitAllowingStateLoss()
        } else {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frameLayoutSearchOrders,
                    DeliveredOrdersDetailsFragment.newInstance(establishmentOrderView),
                    DeliveredOrdersDetailsFragment.TAG
                ).setCustomAnimations(
                    R.anim.slide_enter_from_right,
                    R.anim.slide_exit_to_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                .hide(supportFragmentManager.findFragmentByTag(SearchOrdersFragment.TAG)!!)
                .addToBackStack(DeliveredOrdersDetailsFragment.TAG).commitAllowingStateLoss()
        }

    }

    override fun replaceByInformedOrdersDetailsFragment(
        establishmentOrderView: EstablishmentOrderView,
        isFromOrders: Boolean
    ) {
        llBack.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_LIST_DETAIL

        if (isFromOrders) {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frameLayoutOrders,
                    InformedOrdersDetailsFragment.newInstance(establishmentOrderView),
                    InformedOrdersDetailsFragment.TAG
                ).setCustomAnimations(
                    R.anim.slide_enter_from_right,
                    R.anim.slide_exit_to_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                .hide(supportFragmentManager.findFragmentByTag(OrdersHistory.TAG)!!)
                .addToBackStack(InformedOrdersDetailsFragment.TAG).commitAllowingStateLoss()
        } else {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frameLayoutSearchOrders,
                    InformedOrdersDetailsFragment.newInstance(establishmentOrderView),
                    InformedOrdersDetailsFragment.TAG
                ).setCustomAnimations(
                    R.anim.slide_enter_from_right,
                    R.anim.slide_exit_to_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                .hide(supportFragmentManager.findFragmentByTag(SearchOrdersFragment.TAG)!!)
                .addToBackStack(InformedOrdersDetailsFragment.TAG).commitAllowingStateLoss()
        }


    }

    override fun replaceProductsTemplateFragment() {
        llBack.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = TITLE_ADD_PRODUCTS

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutProducts,
                ProductsFragment.newInstance(),
                ProductsFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(MyProductsFragment.TAG)!!)
            .addToBackStack(ProductsFragment.TAG).commitAllowingStateLoss()
    }


    override fun onBackPressed() {
        checkForVisibleMenuFragment()
    }

    private fun popBackStack() {
        val count = supportFragmentManager.backStackEntryCount
        if (count < 2) {
            finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
            checkForVisibleStackFragment()
        }
    }

    private fun checkForVisibleStackFragment() {
        when {

            isFragmentVisible(SearchOrdersFragment.TAG) -> {
                llBack.visibility = View.VISIBLE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_SEARCH
            }

            isFragmentVisible(MyProductsFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_MY_PRODUCTS
            }

            isFragmentVisible(OrdersHistory.TAG) -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_ORDERS_HISTORY
            }

            isFragmentVisible(ProfileFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_PROFILE
            }

            isFragmentVisible(ApprovedOrdersFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_LIST_DETAIL
            }

            isFragmentVisible(CanceledOrdersDetailsFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_LIST_DETAIL
            }

            isFragmentVisible(DeliveredOrdersDetailsFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_LIST_DETAIL
            }

            isFragmentVisible(InformedOrdersDetailsFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_LIST_DETAIL
            }

            isFragmentVisible(ProductsFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_ADD_PRODUCTS
            }
        }
    }


    private fun checkForVisibleMenuFragment() {
        when (bottom_navigation.selectedItemId) {
            R.id.item_search -> {
                val fragment = supportFragmentManager.findFragmentById(R.id.frameLayoutSearchOrders)
                if (fragment!!.tag == SearchOrdersFragment.TAG) {
                    finish()
                } else {
                    supportFragmentManager.popBackStackImmediate()
                    val newFragment =
                        supportFragmentManager.findFragmentById(R.id.frameLayoutSearchOrders)
                    checkForVisibleFragment(newFragment!!.tag!!)
                }
            }

            R.id.item_orders -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutOrders)
                if (fragment!!.tag == OrdersHistory.TAG) {
                    finish()
                } else {
                    supportFragmentManager.popBackStackImmediate()
                    val newFragment =
                        supportFragmentManager.findFragmentById(R.id.frameLayoutOrders)
                    checkForVisibleFragment(newFragment!!.tag!!)
                }
            }

            R.id.item_products -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutProducts)
                if (fragment!!.tag == MyProductsFragment.TAG) {
                    finish()
                } else {
                    supportFragmentManager.popBackStackImmediate()
                    val newFragment =
                        supportFragmentManager.findFragmentById(R.id.frameLayoutProducts)
                    checkForVisibleFragment(newFragment!!.tag!!)
                }
            }

            R.id.item_profile -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutProfile)
                if (fragment!!.tag == ProfileFragment.TAG) {
                    finish()
                } else {
                    popBackStack()
                }
            }
        }
    }

    private fun isFragmentVisible(tag: String): Boolean {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        fragment?.isVisible.let {
            if (it == null) {
                return false
            }
            return it
        }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.item_search -> {
                    frameLayoutSearchOrders.visibility = View.VISIBLE
                    frameLayoutOrders.visibility = View.GONE
                    frameLayoutProducts.visibility = View.GONE
                    frameLayoutProfile.visibility = View.GONE

                    val fragment = supportFragmentManager
                        .findFragmentById(R.id.frameLayoutSearchOrders)

                    if (fragment == null) {
                        setSearchOrdersFragment()
                    } else {
                        fragment.tag?.let { checkForVisibleFragment(it) }
                    }

                    return@OnNavigationItemSelectedListener true
                }

                R.id.item_orders -> {

                    frameLayoutSearchOrders.visibility = View.GONE
                    frameLayoutOrders.visibility = View.VISIBLE
                    frameLayoutProducts.visibility = View.GONE
                    frameLayoutProfile.visibility = View.GONE

                    val fragment = supportFragmentManager
                        .findFragmentById(R.id.frameLayoutOrders)

                    if (fragment == null) {
                        replaceOrdersHistoryFragments()
                    } else {
                        fragment.tag?.let { checkForVisibleFragment(it) }
                    }
                    return@OnNavigationItemSelectedListener true
                }

                R.id.item_products -> {

                    frameLayoutSearchOrders.visibility = View.GONE
                    frameLayoutOrders.visibility = View.GONE
                    frameLayoutProducts.visibility = View.VISIBLE
                    frameLayoutProfile.visibility = View.GONE

                    val fragment = supportFragmentManager
                        .findFragmentById(R.id.frameLayoutProducts);

                    if (fragment == null) {
                        replaceMyProductsFragments()
                    } else {
                        fragment.tag?.let { checkForVisibleFragment(it) }
                    }
                    return@OnNavigationItemSelectedListener true
                    //}
                }

                R.id.item_profile -> {
                    frameLayoutSearchOrders.visibility = View.GONE
                    frameLayoutOrders.visibility = View.GONE
                    frameLayoutProducts.visibility = View.GONE
                    frameLayoutProfile.visibility = View.VISIBLE

                    val fragment = supportFragmentManager
                        .findFragmentById(R.id.frameLayoutProfile);

                    if (fragment == null) {
                        replaceProfileFragment()
                    } else {
                        fragment.tag?.let { checkForVisibleFragment(it) }
                    }
                    return@OnNavigationItemSelectedListener true
                    //  }
                }
            }
            false
        }


    private fun checkForVisibleFragment(tag: String) {
        when (tag) {
            MyProductsFragment.TAG -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_MY_PRODUCTS
            }

            OrdersHistory.TAG -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_ORDERS_HISTORY
            }

            ProfileFragment.TAG -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_PROFILE
            }

            SearchOrdersFragment.TAG -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_SEARCH
                val fragment = supportFragmentManager.findFragmentById(R.id.frameLayoutSearchOrders) as SearchOrdersFragment
                fragment.refreshList()
            }

            ProductsFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_ADD_PRODUCTS
            }

            ApprovedOrdersFragment.TAG, DeliveredOrdersFragment.TAG,
                CanceledOrdersFragment.TAG, InformedOrdersFragment.TAG -> {
                llBack.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_ORDERS_HISTORY
            }

            ApprovedOrdersDetailsFragment.TAG, DeliveredOrdersDetailsFragment.TAG,
            CanceledOrdersDetailsFragment.TAG, InformedOrdersDetailsFragment.TAG-> {
                llBack.visibility = View.VISIBLE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = TITLE_LIST_DETAIL
            }
        }
    }

    private fun animateLoadingText() {
        if (loadingDots == null) {
            loadingDots =
                LoadingDots(dots)
            loadingDots?.animateLoadingText()
        }
    }

    private fun animateLoadingImage() {
        if (loadingAnimationImageView == null) {
            loadingAnimationImageView =
                LoadingAnimationImageView(
                    ivShrimp
                )
            loadingAnimationImageView?.startAnimation()
        }
    }

    override fun showLoading() {
        layoutLoading.visibility = View.VISIBLE
        animateLoadingText()
        animateLoadingImage()
        bottom_navigation.visibility = View.GONE
    }

    override fun hideLoading() {
        layoutLoading.visibility = View.GONE
        bottom_navigation.visibility = View.VISIBLE
    }


}
