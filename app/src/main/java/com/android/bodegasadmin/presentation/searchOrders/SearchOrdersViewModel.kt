package com.android.bodegasadmin.presentation.searchOrders

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.domain.searchOrders.GetOrdersListByDni
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.searchOrders.mapper.SearchEstablishmentOrderViewMapper
import com.android.bodegasadmin.presentation.searchOrders.model.SearchEstablishmentOrderView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class SearchOrdersViewModel constructor(
    private val getOrdersListByDni: GetOrdersListByDni,
    private val searchEstablishmentOrdersViewMapper: SearchEstablishmentOrderViewMapper
) : ViewModel(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _searchEstablishmentCustomerOrders = MutableLiveData<Resource<List<SearchEstablishmentOrderView>>>()
    val establishmentCustomerOrders: LiveData<Resource<List<SearchEstablishmentOrderView>>> = _searchEstablishmentCustomerOrders

    fun getOrdersListByDniOrNameWithDates(
        establishmentId: Int,
        clientDni: String,
        clientName: String,
        orderStartDate: String,
        orderEndDate: String,
        orderState: String,
        page: String,
        size: String,
        sortBy: String
    ) {
        viewModelScope.launch {
            _searchEstablishmentCustomerOrders.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val establishmentOrdersResult =
                getOrdersListByDni.getOrdersListByDni(establishmentId, clientDni, clientName, orderStartDate, orderEndDate, orderState, page, size, sortBy)

            if (establishmentOrdersResult.status == Status.SUCCESS) {
                val establishmentOrdersResultData = establishmentOrdersResult.data
                if (establishmentOrdersResultData.isNotEmpty()) {
                    val establishmentOrdersView = establishmentOrdersResultData.map { searchEstablishmentOrdersViewMapper.mapToView(it) }
                    _searchEstablishmentCustomerOrders.value = Resource(Status.SUCCESS, establishmentOrdersView, "")
                } else {
                    _searchEstablishmentCustomerOrders.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _searchEstablishmentCustomerOrders.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }
}