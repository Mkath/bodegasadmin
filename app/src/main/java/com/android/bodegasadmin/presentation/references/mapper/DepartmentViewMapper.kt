package com.android.bodegasadmin.presentation.references.mapper

import com.android.bodegasadmin.domain.references.department.Department
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.references.DepartmentView

class DepartmentViewMapper: Mapper<DepartmentView, Department> {
    override fun mapToView(type: Department): DepartmentView {
        return DepartmentView(
            type.id,
            type.name
        )
    }
}