package com.android.bodegasadmin.presentation.searchOrders.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegasadmin.R
import com.android.bodegasadmin.presentation.searchOrders.model.SearchEstablishmentOrderView

class SearchEstablishmentOrderAdapter  (private val viewHolderListener: SearchEstablishmentOrderViewHolder.ViewHolderListener):
RecyclerView.Adapter<SearchEstablishmentOrderViewHolder>() {

    private val customerOrdersList = mutableListOf<SearchEstablishmentOrderView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchEstablishmentOrderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_customer_order_search, parent, false)
        context = parent.context
        return SearchEstablishmentOrderViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        if (customerOrdersList != null)
            return customerOrdersList.size
        return 0
    }

    override fun onBindViewHolder(holder: SearchEstablishmentOrderViewHolder, position: Int) {
        val establishmentOrderView = customerOrdersList[position]
        holder.tvUserName.text = establishmentOrderView.customerCompleteName
        holder.tvTotal.text = String.format("%s %s", "s/ ", establishmentOrderView.total)
        holder.tvDeliveryType.text = establishmentOrderView.deliveryType

        if(establishmentOrderView.isDelivery){
            holder.tvDate.visibility = View.VISIBLE
            holder.tvTitleDate.visibility = View.VISIBLE
            holder.tvDate.text = establishmentOrderView.shippingDateFrom
        }else {
            holder.tvDate.visibility = View.GONE
            holder.tvTitleDate.visibility = View.GONE
        }

        holder.tvStatus.text = establishmentOrderView.stringStatus

    }

    fun setStoreList(poolList: List<SearchEstablishmentOrderView>) {
        this.customerOrdersList.clear()
        this.customerOrdersList.addAll(poolList)
    }
}