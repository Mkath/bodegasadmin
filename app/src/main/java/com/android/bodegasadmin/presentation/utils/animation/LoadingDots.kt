package com.android.bodegasadmin.presentation.utils.animation

import android.os.Handler
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat


class LoadingDots constructor(private val linearLayout: LinearLayout) {

    companion object {
        private const val ANIMATION_DELAY: Long = 750
    }

    private var tempDots: Int = 0
    private var dotsCount: Int = 4
    private var threadHandler: Handler? = null
    private val context = linearLayout.context

    init {
        buildLoadingDots()
    }

    private fun buildLoadingDots() {
        for (i in 0..dotsCount) {
            val dotTextView = TextView(context)
            dotTextView.text = "."
            dotTextView.setTextColor(ContextCompat.getColor(context, android.R.color.white))
            dotTextView.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            dotTextView.visibility = View.INVISIBLE
            linearLayout.addView(dotTextView)
        }
    }


    fun animateLoadingText() {
        if (threadHandler == null) {
            threadHandler = Handler()
            val runnable = object : Runnable {
                override fun run() {
                    threadHandler?.postDelayed(this,
                        ANIMATION_DELAY
                    )
                    if (tempDots == dotsCount) {
                        tempDots = 1
                        hideAllDots()
                    } else {
                        showDot(tempDots)
                        tempDots++
                    }
                }
            }
            runnable.run()
        }
    }

    private fun showDot(dotNo: Int) {
        for (i in 0..dotNo) {
            linearLayout.getChildAt(i).visibility = View.VISIBLE
        }
    }

    private fun hideAllDots() {
        for (i in 1 until linearLayout.childCount) {
            val dotTextView = linearLayout.getChildAt(i)
            dotTextView.visibility = View.INVISIBLE
        }
    }
}