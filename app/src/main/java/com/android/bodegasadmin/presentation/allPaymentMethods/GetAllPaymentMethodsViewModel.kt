package com.android.bodegasadmin.presentation.allPaymentMethods

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.domain.allpaymentmethods.GetAllPaymentMethods
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.allPaymentMethods.mapper.AllPaymentMethodsViewMapper
import com.android.bodegasadmin.presentation.allPaymentMethods.model.AllPaymentMethodsView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class GetAllPaymentMethodsViewModel constructor(
    private val getAllPaymentMethods: GetAllPaymentMethods,
    private val getAllPaymentMethodsViewMapper: AllPaymentMethodsViewMapper
) :
    ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _paymentMethod = MutableLiveData<Resource<List<AllPaymentMethodsView>>>()
    val paymentMethod: LiveData<Resource<List<AllPaymentMethodsView>>> = _paymentMethod

    fun getPaymentMethods() {
        _paymentMethod.value = Resource(Status.LOADING, mutableListOf(), "")
        viewModelScope.launch {
            val allPaymentMethodsResult = getAllPaymentMethods.getAllPaymentMethods()

            if (allPaymentMethodsResult.status == Status.SUCCESS) {
                val allPaymentMethodsResultData = allPaymentMethodsResult.data

                if (allPaymentMethodsResultData.isNotEmpty()) {
                    val allPaymentMethodsViewList =
                        allPaymentMethodsResultData.map { getAllPaymentMethodsViewMapper.mapToView(it) }
                    _paymentMethod.value = Resource(Status.SUCCESS, allPaymentMethodsViewList, "")
                } else {
                    _paymentMethod.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }

            } else {
                _paymentMethod.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }
}
