package com.android.bodegasadmin.presentation.establishment.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EstablishmentLoginTokenView(
    val idEntity: String,
    val token: String
) : Parcelable