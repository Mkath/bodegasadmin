package com.android.bodegasadmin.presentation.products_template.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductsBodyView(
    val productTemplateId: Int,
    val price:String
) : Parcelable