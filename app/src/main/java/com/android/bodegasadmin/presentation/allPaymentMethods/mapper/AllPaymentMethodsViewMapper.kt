package com.android.bodegasadmin.presentation.allPaymentMethods.mapper

import com.android.bodegasadmin.domain.allpaymentmethods.AllPaymentMethod
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.allPaymentMethods.model.AllPaymentMethodsView

class AllPaymentMethodsViewMapper : Mapper<AllPaymentMethodsView, AllPaymentMethod> {

    override fun mapToView(type: AllPaymentMethod): AllPaymentMethodsView {
        return AllPaymentMethodsView(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}