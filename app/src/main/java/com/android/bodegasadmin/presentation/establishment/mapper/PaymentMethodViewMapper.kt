package com.android.bodegasadmin.presentation.establishment.mapper

import com.android.bodegasadmin.domain.paymentMethod.PaymentMethod
import com.android.bodegasadmin.domain.schedule.Schedule
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.establishment.model.PaymentMethodView
import com.android.bodegasadmin.presentation.establishment.model.ScheduleView

class PaymentMethodViewMapper : Mapper<PaymentMethodView, PaymentMethod> {

    override fun mapToView(type: PaymentMethod): PaymentMethodView {
        return PaymentMethodView(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}