package com.android.bodegasadmin.presentation.ordersHistory.adapter

import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_customer_order_detail.view.*

class OrderDetailViewHolder (itemView: View, viewHolderListener: ViewHolderListener):
    RecyclerView.ViewHolder(itemView) {
    val tvProductName : TextView = itemView.tvProductName
    val tvUnitPrice : TextView = itemView.tvUnitPrice
    val checkBox: CheckBox = itemView.checkBox
    val tvSubTotalProduct: TextView = itemView.tvSubTotalProduct
    val tvIsAttended: TextView = itemView.tvIsAttended
    val imageProduct: ImageView = itemView.imageProduct
    val description: TextView = itemView.tvDescription

    init {
        itemView.setOnClickListener {
            viewHolderListener.onClick(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClick(
            position: Int
        )
    }
}