package com.android.bodegasadmin.presentation.ordersHistory

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.allPaymentMethods.GetAllPaymentMethodsViewModel
import com.android.bodegasadmin.presentation.allPaymentMethods.model.AllPaymentMethodsView
import com.android.bodegasadmin.presentation.ordersHistory.adapter.OrderDetailAdapter
import com.android.bodegasadmin.presentation.ordersHistory.adapter.OrderDetailViewHolder
import com.android.bodegasadmin.presentation.ordersHistory.model.DistanceView
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.presentation.utils.DateTimeHelper
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_approved_orders_details.*
import kotlinx.android.synthetic.main.fragment_informed_orders_details.*
import kotlinx.android.synthetic.main.fragment_informed_orders_details.boxDelivery
import kotlinx.android.synthetic.main.fragment_informed_orders_details.boxStatus
import kotlinx.android.synthetic.main.fragment_informed_orders_details.btnAtender
import kotlinx.android.synthetic.main.fragment_informed_orders_details.btnCancel
import kotlinx.android.synthetic.main.fragment_informed_orders_details.imSend
import kotlinx.android.synthetic.main.fragment_informed_orders_details.informedDetailView
import kotlinx.android.synthetic.main.fragment_informed_orders_details.rvCustomerOrdersDetail
import kotlinx.android.synthetic.main.fragment_informed_orders_details.titleDeliveryCharge
import kotlinx.android.synthetic.main.fragment_informed_orders_details.titleSendDate
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvClientName
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvDeliveryCharge
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvDirection
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvFechaUpdate
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvOrderDate
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvPM
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvPhone
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvSendDate
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvTitlePayment
import kotlinx.android.synthetic.main.fragment_informed_orders_details.tvTotalOrder
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.math.BigDecimal

private val ORDER = "orderDetails"

class InformedOrdersDetailsFragment : Fragment(), OrderDetailViewHolder.ViewHolderListener {

    private lateinit var order: EstablishmentOrderView
    private var adapter = OrderDetailAdapter("I",this)
    private val informedOrderViewModel: InformedOrdersViewModel by sharedViewModel()
    private val approvedOrdersViewModel: ApprovedOrdersViewModel by sharedViewModel()
    private val distanceViewModel: DistanceViewModel by sharedViewModel()
    private lateinit var distanceView: DistanceView
    private var distanceInKM = ""
    private var distanceInBlocks =  ""
    private var travelTimeByCar = ""
    private var travelTimeByBike = ""
    private var travelTimeByWalking = ""
    private val DELIVERY_CHARGE_EMPTY=0.0
    private var deliveryChargeForDelivery=0.0
    private var listener: OnFragmentInteractionListener? = null
    private var paymentMethodsViewList = mutableListOf<AllPaymentMethodsView>()
    private val getAllPaymentMethodsViewModel: GetAllPaymentMethodsViewModel by viewModel()
    private var paymentMethodsHashMap : HashMap<Int, String>  = HashMap<Int, String>()
    private val REQUEST_CODE_STORAGE = 200

    companion object {
        const val TAG = "InformedOrdersDetailsFragment"

        @JvmStatic
        fun newInstance(establishmentOrderView: EstablishmentOrderView) =
            InformedOrdersDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ORDER, establishmentOrderView)
                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            order = it.getParcelable(ORDER)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_informed_orders_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
        setOnClickListener()
        observerUpdateOrder()
        observerGetDistanceInformation()
    }

    private fun setOnClickListener() {
        btnAtender.setOnClickListener {
            if(seRequiereIngresarMontoPorDelivery()) {
                Snackbar.make(
                    informedDetailView,
                    "Digite el monto por delivery a cobrar",
                    Snackbar.LENGTH_LONG
                ).show()
            }
            else if (validateCheckProducts()) {
               informedOrderViewModel.updateStateOrder(deliveryChargeForDelivery, order.orderId.toInt(), "A", adapter.getList())
            } else {
                Snackbar.make(
                    informedDetailView,
                    "No ha seleccionado ningún producto como atendido",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }

        btnCancel.setOnClickListener {
            informedOrderViewModel.updateStateOrder(DELIVERY_CHARGE_EMPTY,order.orderId.toInt(), "R", adapter.getList())
        }


        boxDelivery.setOnClickListener {
            if(order.deliveryType == "Delivery") {
                mostrarVentanaDetalleDelivery()
            }
        }
    }

    private fun validateCheckProducts(): Boolean {
        val list = adapter.getList()
        var isChecked = false
        for (ordersItemView in list) {
            if (ordersItemView.isAttended) {
                isChecked = true
            }
        }
        return isChecked
    }

    private fun setButtonPhoneIconToCall() {
        imPhoneInformado.setOnClickListener {
            if (validateCallPermissionsStorage()) {
                makeCallWhenClickOn()
            } else {
                requestCallPermissions()
            }
        }
    }

    private fun validateCallPermissionsStorage(): Boolean {
        return ActivityCompat.checkSelfPermission(
            activity!!.applicationContext,
            android.Manifest.permission.CALL_PHONE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestCallPermissions() {
        val contextProvider =
            ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                android.Manifest.permission.CALL_PHONE
            )

        if (contextProvider) {
            Toast.makeText(
                activity!!.applicationContext,
                "Los permisos son requeridos para llamar al cliente",
                Toast.LENGTH_SHORT
            ).show()
        }
        permissionRequest()
    }

    private fun permissionRequest() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(android.Manifest.permission.CALL_PHONE),
            REQUEST_CODE_STORAGE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    makeCallWhenClickOn()
                } else {
                    Toast.makeText(
                        activity!!.applicationContext,
                        "No aceptó los permisos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

    }

    private fun makeCallWhenClickOn() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:"+order.customerPhoneNumber))
        startActivity(intent)
    }

    private fun setView() {
        tvClientName.text = order.customerCompleteName
        tvPhone.text = order.customerPhoneNumber
        tvOrderDate.text = DateTimeHelper.parseDateOrder(order.creationDate)
        tvFechaUpdate.text = DateTimeHelper.parseDateOrder(order.updateDate)
        setButtonPhoneIconToCall()
        if(order.isDelivery){
            tvSendDate.visibility = View.VISIBLE
            titleSendDate.visibility = View.VISIBLE
            imSend.visibility = View.VISIBLE
            tvSendDate.text = order.shippingDateFrom
        }else{
            tvSendDate.visibility = View.GONE
            titleSendDate.visibility = View.GONE
            imSend.visibility = View.GONE
        }
        tvDirection.text = order.customerAddress + "\n" + order.customerUrbanization
        tvTotalOrder.text = String.format("%s %s", "s/", order.total)
        tvPM.text = order.paymentMethodDescription

        var aux: BigDecimal = order.customerAmount.toBigDecimal() - order.total.toBigDecimal()
        aux = String.format("%.2f", aux).toBigDecimal()
        var inputTotalVuelto10 =String.format("%s %s", "s/ ",  aux.toString())

        if (order.paymentMethodDescription == "Efectivo"){
            tvTitlePayment.visibility = View.VISIBLE
            var paga1 = String.format("%s %s %s", "Paga con:"," s/", order.customerAmount)
            tvTitlePayment.text = paga1 + " Vuelto: " + inputTotalVuelto10
        }else{
            tvTitlePayment.visibility = View.GONE
            tvTitlePayment.text = ""
        }

        boxDelivery.text = order.deliveryType
        boxDelivery.isEnabled = order.deliveryType == "Delivery"
        boxStatus.text = String.format("%s %s", "Pedido", order.stringStatus)

        if (order.isDelivery){
            titleDeliveryCharge.visibility = View.VISIBLE
            tvDeliveryCharge.visibility = View.VISIBLE
            if (order.deliveryCharge != "0.00"){
                tvDeliveryCharge.text = String.format("%s %s", "s/", order.deliveryCharge )
            }
        }else{
            titleDeliveryCharge.visibility = View.GONE
            tvDeliveryCharge.visibility = View.GONE
        }
        setRecyclerView()
    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvCustomerOrdersDetail.layoutManager = linearLayoutManager
        rvCustomerOrdersDetail.adapter = adapter
        adapter.setStoreList(order.orderDetails)
    }

    private fun observerUpdateOrder(){
        informedOrderViewModel.updateOrder.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    informedOrderViewModel.getOrdersListByEstablishmentAndState(
                        PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                    "I")
                    approvedOrdersViewModel.getOrdersListByEstablishmentAndState(
                        PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                        "A"
                    )
                    informedOrderViewModel.cleanUpdateState()
                    listener!!.hideLoading()
                    listener!!.onBackPressed()
                }
                Status.ERROR -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        informedDetailView,
                        "Ocurrió un error al actualizar su pedido, intente nuevamente por favor",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    if (it.data) {
                        listener!!.showLoading()
                    } else {
                        listener!!.hideLoading()
                    }
                }
            }
        })

    }

    private fun observerGetDistanceInformation() {
        distanceViewModel.distance.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                Status.SUCCESS -> {
                    distanceView = it.data
                    getDataInformation()
                }

                Status.ERROR -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        informedDetailView,
                        "Ocurrió un error al calcular la distancia, intente nuevamente por favor",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    if (it.data.toString().isEmpty()) {
                        listener!!.showLoading()
                    } else {
                        listener!!.hideLoading()
                    }
                }
            }
        })
        distanceViewModel.getDistance(PreferencesManager.getInstance().getEstablishmentSelected().latitude,
                                      PreferencesManager.getInstance().getEstablishmentSelected().longitude,
                                      order.customerLatitude,
                                      order.customerLongitude)
    }

    private fun getDataInformation() {
        Log.e("debug", "getDataInformation: ....")
        distanceInKM = distanceView.distanceInKms
        distanceInBlocks = distanceView.numberOfBlocks.toString()
        travelTimeByCar = distanceView.travelTimeByCar
        travelTimeByBike = distanceView.travelTimeByBike
        travelTimeByWalking = distanceView.travelTimeByWalking
        boxDelivery.isEnabled = true
    }

    override fun onClick(position: Int) {
    }

    interface OnFragmentInteractionListener {
        fun onBackPressed()
        fun showLoading()
        fun hideLoading()
    }

    private fun  mostrarVentanaDetalleDelivery() {
        val dialog = Dialog(context!!)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_dialog_charge_for_delivery)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        val valorAddress = dialog.findViewById(R.id.tvDirection) as TextView
        valorAddress.text = order.customerAddress
        val valorDistanceInKm = dialog.findViewById(R.id.tvValorDistanciaEnKm) as TextView
        valorDistanceInKm.text = distanceInKM
        val valorDistanceInBlocks = dialog.findViewById(R.id.tvTipoEstablecimientoValue) as TextView
        valorDistanceInBlocks.text = distanceInBlocks
        val valorTimeByCar = dialog.findViewById(R.id.tvTiempoViajeAutoValue) as TextView
        valorTimeByCar.text = travelTimeByCar
        val valorTimeByBike = dialog.findViewById(R.id.tvTiempoViajeBicicletaValue) as TextView
        valorTimeByBike.text = travelTimeByBike
        val valorTimeByWalking = dialog.findViewById(R.id.tvTiempoViajePieValue) as TextView
        valorTimeByWalking.text = travelTimeByWalking
        val btnValueChargeForDelivey = dialog.findViewById(R.id.btnValueChargeForDelivey) as Button
        val boxMontoPorDelivery = dialog.findViewById(R.id.boxMontoPorDelivery) as TextInputLayout
        val etMontoPorDelivery = dialog.findViewById(R.id.etMontoPorDelivery) as TextInputEditText

        if(PreferencesManager.getInstance().getEstablishmentSelected().deliveryCharge) {
            btnValueChargeForDelivey.setOnClickListener {
                if(etMontoPorDelivery.text.isNullOrEmpty()) {
                    boxMontoPorDelivery.error="Digita una cantidad válida"
                }
                else {
                    deliveryChargeForDelivery = etMontoPorDelivery.text.toString().toDouble()
                    tvDeliveryCharge.visibility = View.VISIBLE
                    titleDeliveryCharge.visibility = View.VISIBLE
                    tvDeliveryCharge.text = String.format("%s %s", "s/", etMontoPorDelivery.text.toString())
                    hideKeyboardAfterClickOnButton()
                    dialog.dismiss()
                }
            }
        }
        else {
            boxMontoPorDelivery.isEnabled= false
            btnValueChargeForDelivey.isEnabled= false
        }

        dialog.show()
    }

    fun hideKeyboard() {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0);
    }

    fun isOpenKeyboard(): Boolean {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.isAcceptingText
    }

    fun hideKeyboardAfterClickOnButton()  {
        if (isOpenKeyboard()){
            hideKeyboard()
        }
    }

    private fun seRequiereIngresarMontoPorDelivery(): Boolean {
        //charge for delivery del establishment es true
        //el pedido es "delivery"
        //el estado es "Informado"
         if((PreferencesManager.getInstance().getEstablishmentSelected().deliveryCharge) && order.deliveryType == "Delivery" && order.status == "I") {
             return deliveryChargeForDelivery<0.0
        }
         else return false
    }
}