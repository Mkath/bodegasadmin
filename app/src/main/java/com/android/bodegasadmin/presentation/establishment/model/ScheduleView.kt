package com.android.bodegasadmin.presentation.establishment.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleView(
    val range: String,
    val startTime: String ? = "",
    val endTime: String ? = "",
    val shippingScheduleId:Int
) : Parcelable