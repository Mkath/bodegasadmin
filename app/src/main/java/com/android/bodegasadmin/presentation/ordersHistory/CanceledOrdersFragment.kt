package com.android.bodegasadmin.presentation.ordersHistory

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.presentation.customerOrders.adapter.EstablishmentOrderAdapter
import com.android.bodegasadmin.presentation.ordersHistory.model.EstablishmentOrderView
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.ordersHistory.adapter.EstablishmentOrderViewHolder
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_canceled_orders.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CanceledOrdersFragment  : Fragment() , EstablishmentOrderViewHolder.ViewHolderListener {

    private var listener: OnFragmentInteractionListener? = null
    private val establishmentOrdersViewModel: EstablishmentOrdersViewModel by viewModel()
    private var establishmentOrdersViewList: MutableList<EstablishmentOrderView> = mutableListOf()
    private var adapter = EstablishmentOrderAdapter(this)

    companion object {
        const val TAG = "CanceledOrdersFragment"
        const val ESTADO_RECHAZADO ="R"
        const val ESTADO_ANULADO ="N"

        @JvmStatic
        fun newInstance() =
            CanceledOrdersFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

   /* override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){
            establishmentOrdersViewModel.getOrdersListByEstablishmentAndState(
                PreferencesManager.getInstance().getEstablishmentSelected().storeId, ESTADO_RECHAZADO)
        }
    }*/

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_canceled_orders, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnRechazado.setOnClickListener{
            btnRechazado.background = resources.getDrawable( R.drawable.button_rounded_purple_background)
            btnAnulado.background = resources.getDrawable( R.drawable.button_rounded_grey_background)
            getCanceledOrders(ESTADO_RECHAZADO)
        }

        btnAnulado.setOnClickListener{
            btnRechazado.background = resources.getDrawable( R.drawable.button_rounded_grey_background)
            btnAnulado.background =resources.getDrawable( R.drawable.button_rounded_purple_background)
            getCanceledOrders(ESTADO_ANULADO)
        }
    }

    private fun getCanceledOrders(state: String) {
        establishmentOrdersViewModel.establishmentCustomerOrders.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    establishmentOrdersViewList = it.data.toMutableList()
                    if(establishmentOrdersViewList.isEmpty()){
                        rvListaCancelados.visibility=View.GONE;
                        containerEmptyState.visibility=View.VISIBLE
                    }

                    else{
                        rvListaCancelados.visibility=View.VISIBLE;
                        containerEmptyState.visibility=View.GONE
                        setRecyclerView()
                    }
                }

                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        listaCanceladosView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        establishmentOrdersViewModel.getOrdersListByEstablishmentAndState(
            PreferencesManager.getInstance().getEstablishmentSelected().storeId, state) //PreferencesManager.getInstance().getUser().customerId

    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvListaCancelados.layoutManager = linearLayoutManager
        rvListaCancelados.adapter = adapter
        adapter.setStoreList(establishmentOrdersViewList)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    fun refreshList(){
        establishmentOrdersViewModel.getOrdersListByEstablishmentAndState(
            PreferencesManager.getInstance().getEstablishmentSelected().storeId, ESTADO_RECHAZADO)
    }

    override fun onResume() {
        super.onResume()
        getCanceledOrders(ESTADO_RECHAZADO)
    }

    override fun onClick(position: Int) {
        val classView = establishmentOrdersViewList[position]
        listener?.replaceByCanceledDetailsOrderFragment(classView, true)
    }

    interface OnFragmentInteractionListener {
        fun replaceByCanceledDetailsOrderFragment(establishmentOrderView: EstablishmentOrderView, isFromOrders: Boolean)
        fun showLoading()
        fun hideLoading()
    }

}