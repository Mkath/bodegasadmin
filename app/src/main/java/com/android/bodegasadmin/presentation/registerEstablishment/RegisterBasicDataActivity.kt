package com.android.bodegasadmin.presentation.registerEstablishment

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.android.bodegasadmin.R
import com.android.bodegasadmin.data.PreferencesManager
import com.android.bodegasadmin.presentation.BaseActivity
import com.android.bodegasadmin.presentation.establishment.model.RegisterEstablishmentBodyView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_register_basic_data.*

class RegisterBasicDataActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_basic_data)
        setOnClickListener()
    }

    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    private fun setOnClickListener() {
        btnNext.setOnClickListener {
            if (validateExtension(inputname, boxname, 2) &&
                validateExtension(input_ape_paterno, box_ape_paterno, 2) &&
                validateExtension(input_ape_materno, box_ape_materno, 2) &&
                validationEmail() && validationPasswords()
            ) {
                nextViewSuccessfully()
            }
        }
    }

    private fun nextViewSuccessfully() {
        saveDataInLocalStorage()
        nextActivity(null, RegisterIdentityActivity::class.java, true)
    }

    private fun validateExtension(
        input: TextInputEditText,
        inputTitle: TextInputLayout,
        minimumLength: Int
    ): Boolean {
        val campo = input.text.toString()
        inputTitle.error = null
        if ((campo.isEmpty()) || campo.length < minimumLength) {
            inputTitle.error = "Introduce un dato más extenso."
            return false
        }
        return true
    }

    private fun validationEmail(): Boolean {
        return if (!isEmailValid(inputcorreo.text.toString())) {
            boxcorreo.isErrorEnabled = true
            boxcorreo.error = "Por favor, ingrese un correo válido"
            boxcorreo.boxBackgroundColor = ContextCompat.getColor(
                applicationContext,
                R.color.background_error_color
            )
            false
        } else true
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun validateInputPass(pass: TextInputLayout, inputPass: TextInputEditText): Boolean {
        return if (inputPass.text.toString().length >= 6 && inputPass.text.toString().length <= 12) {
            true
        } else {
            pass.isErrorEnabled = true
            pass.error = applicationContext.getString(R.string.error_pass)
            pass.boxBackgroundColor = ContextCompat.getColor(
                applicationContext,
                R.color.background_error_color
            )
            false
        }
    }

    private fun validationPasswords(): Boolean {
        if (validateInputPass(boxFirstPassword, inputFirstPassword) && validateInputPass(
                boxSecondPassword,
                inputSecondPassword
            )
        ) {
            if (inputFirstPassword.text.toString() == inputSecondPassword.text.toString()) {
                return true
            }
        }
        return false
    }

    private fun saveDataInLocalStorage() {
        val registerBody = RegisterEstablishmentBodyView(
            "",
            "",
            "",
            "",
            "",
            "",
            "S",
            "",
            "",
            "",
            inputcorreo.text.toString(),
            1,
            input_ape_materno.text.toString(),
            input_ape_paterno.text.toString(),
            0.0,
            0.0,
            inputname.text.toString(),
            mutableListOf(),
            inputFirstPassword.text.toString(),
            "",
            "",
            "",
            mutableListOf(),
            "",
            false,
            mutableListOf()
        )

        PreferencesManager.getInstance().saveBodyEstablishment(registerBody)
    }

    private fun loadDataFromLocalStorage() {
        val registerBody = PreferencesManager.getInstance().getBodyEstablishment()
         if (!registerBody.name.equals("")) inputname.setText(registerBody.name)
         if (!registerBody.lastNamePaternal.equals("")) input_ape_paterno.setText(registerBody.lastNamePaternal)
         if (registerBody.lastNameMaternal != "") input_ape_materno.setText(registerBody.lastNameMaternal)
         if (registerBody.email != "") inputcorreo.setText(registerBody.email)
    }

}
