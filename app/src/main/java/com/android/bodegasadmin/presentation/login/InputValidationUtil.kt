package com.android.bodegasadmin.presentation.login

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageView
import android.widget.ScrollView
import com.android.bodegasadmin.R


class InputValidationUtil constructor(
    private val inputEmail: TextInputEditText,
    private val boxEmail: TextInputLayout,
    private val inputPass: TextInputEditText,
    private val boxPass: TextInputLayout,
    private val context: Context,
    private val btnLogin: Button,
    private val imageView: ImageView
) {
    init {
        onFocusChangeListener()
        onTextChangeListener()
    }

    private fun onFocusChangeListener() {

        inputEmail.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
            if (!b) {
                validationInputEmail()
            } else {
                if (boxEmail.isErrorEnabled) {
                    boxEmail.isErrorEnabled = false
                    boxEmail.boxBackgroundColor = ContextCompat.getColor(
                        context,
                        R.color.background_light
                    )
                }
            }
        }

        inputPass.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
            if (!b) {
                validateInputPass()
            } else {
                if (boxPass.isErrorEnabled) {
                    boxPass.isErrorEnabled = false
                    boxPass.boxBackgroundColor = ContextCompat.getColor(
                        context,
                        R.color.background_light
                    )
                }

            }
        }
    }

    private fun onTextChangeListener() {

        btnLogin.isEnabled =
            !(inputEmail.text.toString().isEmpty() && inputPass.text.toString().isEmpty())


        inputEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                btnLogin.isEnabled = inputEmail.text.toString().isNotEmpty() && inputPass.text.toString().isNotEmpty()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (boxEmail.isErrorEnabled) {
                    boxEmail.isErrorEnabled = false
                    boxEmail.boxBackgroundColor = ContextCompat.getColor(
                        context,
                        R.color.background_light
                    )
                }

            }
        })

        inputPass.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                btnLogin.isEnabled = inputEmail.text.toString().isNotEmpty() && inputPass.text.toString().isNotEmpty()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (boxPass.isErrorEnabled) {
                    boxPass.isErrorEnabled = false
                    boxPass.boxBackgroundColor = ContextCompat.getColor(
                        context,
                        R.color.background_light
                    )
                }
            }
        })

    }

    fun getValidate(): Boolean {
        return if (validationInputEmail()) {
            validateInputPass()
        } else {
            false
        }
    }

    private fun validationInputEmail(): Boolean {
        if (inputEmail.text.toString().isNotEmpty()) {
            return if (isEmailValid(inputEmail.text.toString())) {
                true
            } else {
                boxEmail.isErrorEnabled = true
                boxEmail.error = context.getString(R.string.error_email)
                boxEmail.boxBackgroundColor = ContextCompat.getColor(
                    context,
                    R.color.background_error_color
                )

                false
            }

        } else {
            boxEmail.isErrorEnabled = true
            boxEmail.error = "Por favor, ingrese un usuario"
            boxEmail.boxBackgroundColor = ContextCompat.getColor(
                context,
                R.color.background_error_color
            )

            btnLogin.isEnabled = false
            return false
        }

        return true

    }

    private fun validateInputPass(): Boolean {
        if (inputPass.text.toString().isNotEmpty()) {
            if (inputPass.text!!.length < 6) {
                boxPass.isErrorEnabled = true
                boxPass.error = context.getString(R.string.error_pass)
                boxPass.boxBackgroundColor = ContextCompat.getColor(
                    context,
                    R.color.background_error_color
                )
                return false
            } else {
                if (inputPass.text!!.length > 12) {
                    boxPass.isErrorEnabled = true
                    boxPass.error = context.getString(R.string.error_pass)
                    boxPass.boxBackgroundColor = ContextCompat.getColor(
                        context,
                        R.color.background_error_color
                    )
                    return false
                }
            }
        } else {
            boxPass.isErrorEnabled = true
            boxPass.error = "Por favor, ingrese su contraseña"
            boxPass.boxBackgroundColor = ContextCompat.getColor(
                context,
                R.color.background_error_color
            )
            btnLogin.isEnabled = false
            return false
        }

        return true

    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

}
