package com.android.bodegasadmin.presentation.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateTimeHelper {

    companion object {
        fun parseDate(date: String): String {
            try {
                if (date.isNotEmpty()) {
                    val parser = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val formatter = SimpleDateFormat("dd/MM/yy", Locale.getDefault())
                    return formatter.format(parser.parse(date))
                }
                return ""
            } catch (e: ParseException) {
                e.printStackTrace()
                return ""
            }
        }

        fun parseCalendar(calendar: Calendar): String {
            return try {
                val format = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
                format.format(calendar.time)
            } catch (e: ParseException) {
                e.printStackTrace()
                ""
            }
        }

        fun parseCalendarToSaveOrder(calendar: Calendar): String {
            return try {
                val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                format.format(calendar.time)
            } catch (e: ParseException) {
                e.printStackTrace()
                ""
            }
        }

        fun parseCalendarToGetHour(calendar: Calendar): String {
            return try {
                val formatterHour = SimpleDateFormat("hh:mm", Locale.getDefault())
                formatterHour.format(calendar.time)
            } catch (e: ParseException) {
                e.printStackTrace()
                ""
            }
        }

        fun parseFinalDate(date: String): String {
            try {
                if (date.isNotEmpty()) {
                    val parser = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
                    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale("es", "ES"))
                    return formatter.format(parser.parse(date))
                }
                return ""
            } catch (e: ParseException) {
                e.printStackTrace()
                return ""
            }
        }


        fun parseCompleteDate(date: String): String {
            try {
                if (date.isNotEmpty()) {
                    val parser = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val formatter = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
                    return formatter.format(parser.parse(date))
                }
                return ""
            } catch (e: ParseException) {
                e.printStackTrace()
                return ""
            }
        }

        fun parseDateOrder(date: String): String {
            try {
                if (date.isNotEmpty()) {
                    val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault())

                    val formatterHour = SimpleDateFormat("dd MMM yyyy hh:mm aa", Locale.getDefault())
                    return formatterHour.format(parser.parse(date))
                }
                return ""
            } catch (e: ParseException) {
                e.printStackTrace()
                return ""
            }
        }
    }
}