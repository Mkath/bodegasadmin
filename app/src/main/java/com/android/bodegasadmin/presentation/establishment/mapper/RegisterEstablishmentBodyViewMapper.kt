package com.android.bodegasadmin.presentation.establishment.mapper

import com.android.bodegasadmin.domain.establishment.register.RegisterEstablishmentBody
import com.android.bodegasadmin.presentation.establishment.model.RegisterEstablishmentBodyView

open class RegisterEstablishmentBodyViewMapper :
    SaveViewMapper<RegisterEstablishmentBodyView, RegisterEstablishmentBody> {

    override fun mapToView(type: RegisterEstablishmentBody): RegisterEstablishmentBodyView {
        return RegisterEstablishmentBodyView(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.delivery,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.establishmentType,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.operationSchedule,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.shippingSchedule,
            type.urbanization,
            type.deliveryCharge,
            type.paymentMethod
        )
    }

    override fun mapToUseCase(type: RegisterEstablishmentBodyView): RegisterEstablishmentBody {
        return RegisterEstablishmentBody(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.delivery,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.establishmentType,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.operationSchedule,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.shippingSchedule,
            type.urbanization,
            type.deliveryCharge,
            type.paymentMethod
        )
    }
}