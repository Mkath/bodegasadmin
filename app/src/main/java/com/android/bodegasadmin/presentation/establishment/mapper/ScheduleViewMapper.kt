package com.android.bodegasadmin.presentation.establishment.mapper

import com.android.bodegasadmin.domain.schedule.Schedule
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.establishment.model.ScheduleView


class ScheduleViewMapper : Mapper<ScheduleView, Schedule> {

    override fun mapToView(type: Schedule): ScheduleView {
        return ScheduleView(
            type.range,
            type.startTime,
            type.endTime,
            type.shippingScheduleId
        )
    }

}