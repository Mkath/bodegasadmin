package com.android.bodegasadmin.presentation.ordersHistory.mapper

import com.android.bodegasadmin.domain.distance.Distance
import com.android.bodegasadmin.domain.establishmentOrders.OrderDetailBody
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.ordersHistory.model.DistanceView
import com.android.bodegasadmin.presentation.ordersHistory.model.OrderDetailBodyView

class DistanceViewMapper : Mapper<DistanceView, Distance> {

    override fun mapToView(type: Distance): DistanceView {
        return DistanceView(
            type.distanceInKms,
            type.numberOfBlocks,
            type.travelTimeByCar,
            type.travelTimeByBike,
            type.travelTimeByWalking
        )
    }

}