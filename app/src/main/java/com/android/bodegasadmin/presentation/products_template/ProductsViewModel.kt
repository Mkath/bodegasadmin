package com.android.bodegasadmin.presentation.products_template

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegasadmin.domain.products_template.AddProducts
import com.android.bodegasadmin.domain.products_template.GetProductsTemplate
import com.android.bodegasadmin.domain.util.Resource
import com.android.bodegasadmin.domain.util.Status
import com.android.bodegasadmin.presentation.products_template.mapper.ProductsBodyViewMapper
import com.android.bodegasadmin.presentation.products_template.mapper.ProductsViewMapper
import com.android.bodegasadmin.presentation.products_template.model.ProductsView
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ProductsViewModel constructor(
    private val getProductsTemplate: GetProductsTemplate,
    private val productsViewMapper: ProductsViewMapper,
    private val addNewProducts: AddProducts,
    private val productsBodyViewMapper: ProductsBodyViewMapper
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _listProducts = MutableLiveData<Resource<List<ProductsView>>>()
    val listProducts: LiveData<Resource<List<ProductsView>>> = _listProducts

    private val _addProducts = MutableLiveData<Resource<Boolean>>()
    val addProducts: LiveData<Resource<Boolean>> = _addProducts

    fun getProductsTemplate(text: String) {
        viewModelScope.launch {
            _listProducts.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val productsResult =
                getProductsTemplate.getProductsTemplate(text)

            if (productsResult.status == Status.SUCCESS) {

                val productsResultData = productsResult.data

                if (productsResultData.isNotEmpty()) {
                    val suppliersView = productsResultData.map { productsViewMapper.mapToView(it) }
                    _listProducts.value = Resource(Status.SUCCESS, suppliersView, "")

                } else {
                    _listProducts.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _listProducts.value =
                    Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun addProducts(productId:Int, productPrice: String){
        _addProducts.value =
            Resource(Status.LOADING, false, "")
        viewModelScope.launch {
            val addProductsResult =
                addNewProducts.addProducts(productId, productPrice)
            _addProducts.value = Resource(addProductsResult.status, addProductsResult.data, addProductsResult.message)
        }
    }


}