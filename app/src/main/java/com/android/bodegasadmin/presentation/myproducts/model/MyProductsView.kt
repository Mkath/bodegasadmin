package com.android.bodegasadmin.presentation.myproducts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MyProductsView(
    val storeProductId: Int,
    val price: Double,
    val status: String,
    val code: String,
    val name: String,
    val description: String,
    val unitMeasure: String,
    val stock:String,
    val imageProduct: String?=""
) : Parcelable