package com.android.bodegasadmin.presentation.myproducts.mapper

import com.android.bodegasadmin.domain.my_products.MyProducts
import com.android.bodegasadmin.presentation.Mapper
import com.android.bodegasadmin.presentation.myproducts.model.MyProductsView

class MyProductsViewMapper : Mapper<MyProductsView, MyProducts> {

    override fun mapToView(type: MyProducts): MyProductsView {
        return MyProductsView(
            type.storeProductId,
            type.price,
            type.status,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            getStock(type.stock),
            type.imageProduct
        )
    }

    private fun getStock(value: String):String{
       return if(value == "N"){
            "AGOTADO"
        }else{
           ""
       }
    }

}