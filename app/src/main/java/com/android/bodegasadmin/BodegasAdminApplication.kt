package com.android.bodegasadmin

import android.app.Application
import com.android.bodegasadmin.di.*

import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BodegasAdminApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@BodegasAdminApplication)
            modules(
                listOf(
                    appModule,
                    departmentModule,
                    provinceModule,
                    districtModule,
                    establishmentModule,
                    allSchedulesModule,
                    establishmentOrders,
                    productsTemplateModule,
                    distance,
                    myProductsModule,
                    searchOrders,
                    allPaymentMethodsModule,
                    deliveryOrders
                )
            )
        }
    }
}

