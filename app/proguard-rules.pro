# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keepclassmembers class com.android.bodegasadmin.data.network.allPaymentMethods.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.allschedules.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.distance.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.establishment.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.establishmentOrders.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.my_products.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.products_template.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.references.departments.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.references.provinces.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.references.district.model.** { *; }
-keepclassmembers class com.android.bodegasadmin.data.network.searchOrders.model.** { *; }
